﻿using HAJE.WowArena.Simulation;
using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace HAJE.WowArena.Network
{
    public class Session : IUpdatable
    {
        public static Session RunningSession { get; private set; }

        public void Start(NetServer server, NetClient client)
        {
            Debug.Assert(RunningSession == null);
            RunningSession = this;

            arenaScene = new Scene.ArenaScene();
            arenaScene.Start();

            this.log = GameSystem.Log;
            this.server = server;
            this.client = client;

            GameSystem.UpdateLoop.Register(this);

            if (IsHost)
            {
                scheduler.ScheduleOnce(ShowReadyButton, (Second)1.0f);

                foreach (var conn in server.Connections)
                {
                    if (conn.RemoteEndPoint.Address.ToString() == "127.0.0.1")
                    {
                        hostConnection = conn;
                    }
                }
            }
        }

        public void End()
        {
            Debug.Assert(RunningSession == this);
            RunningSession = null;

            GameSystem.UpdateLoop.Hide(this);

            if (server != null)
            {
                server.Shutdown("Session End");
                server = null;
            }
            client.Shutdown("Session End");
            client = null;

            arenaScene.End();
            new Network.SessionCreationService().Start();
        }

        public bool IsHost
        {
            get
            {
                return server != null;
            }
        }

        public void Update(GameTime gameTime)
        {
            NetIncomingMessage msg;
            if (server != null)
            {
                while ((msg = server.ReadMessage()) != null)
                {
                    switch (msg.MessageType)
                    {
                        case NetIncomingMessageType.DebugMessage:
                        case NetIncomingMessageType.ErrorMessage:
                        case NetIncomingMessageType.WarningMessage:
                        case NetIncomingMessageType.VerboseDebugMessage:
                        case NetIncomingMessageType.DiscoveryRequest:
                        case NetIncomingMessageType.UnconnectedData:
                            try
                            {
                                log.WriteLine("처리되지 않은 서버 메세지: " + msg.ReadString());
                            }
                            catch {}
                            break;
                        case NetIncomingMessageType.StatusChanged:
                            NetConnectionStatus status = (NetConnectionStatus)msg.ReadByte();
                            string reason = msg.ReadString();
                            log.WriteLine("서버 상태 변경: " + reason);

                            if (server.ConnectionsCount < 2)
                            {
                                log.WriteLine("세션이 종료되었습니다.");
                                End();
                                return;
                            }

                            break;
                        case NetIncomingMessageType.ConnectionApproval:
                            log.WriteLine("접속 요청: " + msg.SenderEndPoint);
                            msg.SenderConnection.Deny();
                            log.WriteLine("접속 요청 거절 됨");
                            break;
                        case NetIncomingMessageType.Data:
                            log.WriteLine("서버 데이터 수신 됨");
                            CSPacketCode code = (CSPacketCode)msg.ReadByte();
                            ServerMessageHandler(msg, code);
                            break;
                    }
                }
            }

            while ((msg = client.ReadMessage()) != null)
            {
                switch (msg.MessageType)
                {
                    case NetIncomingMessageType.DebugMessage:
                    case NetIncomingMessageType.ErrorMessage:
                    case NetIncomingMessageType.WarningMessage:
                    case NetIncomingMessageType.VerboseDebugMessage:
                    case NetIncomingMessageType.DiscoveryResponse:
                    case NetIncomingMessageType.UnconnectedData:
                        try
                        {
                            log.WriteLine("처리되지 않은 클라 메세지: " + msg.ReadString());
                        }
                        catch { }
                        break;
                    case NetIncomingMessageType.StatusChanged:
                        NetConnectionStatus status = (NetConnectionStatus)msg.ReadByte();
                        string reason = msg.ReadString();
                        log.WriteLine("클라 상태 변경: " + reason);

                        if (client.ServerConnection == null
                            || client.ServerConnection.Status != NetConnectionStatus.Connected)
                        {
                            log.WriteLine("세션이 종료되었습니다.");
                            End();
                            return;
                        }

                        break;
                    case NetIncomingMessageType.Data:
                        SCPacketCode code = (SCPacketCode)msg.ReadByte();
                        ClientMessageHandler(msg, code);
                        break;
                }
            }

            if (simulation != null)
            {
                simulation.Update(gameTime);
            }

            ExecuteCommandQueue(gameTime);
            scheduler.Update(gameTime);
        }

        #region Server Packet Dispatch

        void ServerMessageHandler(NetIncomingMessage msg, CSPacketCode code)
        {
            switch (code)
            {
                case CSPacketCode.SetReady:
                    onSetReady(msg);
                    break;
                case CSPacketCode.Upgrade:
                    onUpgrade(msg);
                    break;
                case CSPacketCode.SetTarget:
                    onSetTarget(msg);
                    break;
                case CSPacketCode.UseSkill:
                    onUseSkill(msg);
                    break;
                default:
                    log.WriteLine("알 수 없는 CSPacketCode: " + (int)code);
                    break;
            };
        }

        void ClientMessageHandler(NetIncomingMessage msg, SCPacketCode code)
        {
            switch (code)
            {
                case SCPacketCode.ShowReadyButton:
                    onShowReadyButton(msg);
                    break;
                case SCPacketCode.StartGame:
                    onStartGame(msg);
                    break;
                case SCPacketCode.GameTick:
                    onGameTickMessage(msg);
                    break;
                default:
                    log.WriteLine("알 수 없는 SCPacketCode: " + (int)code);
                    break;
            };
        }

        #endregion

        #region Client --> Server

        public void SetReady(bool ready)
        {
            var msg = client.CreateMessage();
            msg.Write((byte)CSPacketCode.SetReady);
            msg.Write(ready);
            client.SendMessage(msg, NetDeliveryMethod.ReliableOrdered);
        }

        void onSetReady(NetIncomingMessage msg)
        {
            var isReady = msg.ReadBoolean();
            var player = ResolveSender(msg);
            ready[player] = isReady;

            // 모든 플레이어가 준비 완료이면 시뮬레이션 시작
            for (int i = 0; i < Player.Max; i++)
            {
                if (!ready[i]) return;
            }

            StartGame();
        }

        public void Upgrade(int character)
        {
            var msg = client.CreateMessage();
            msg.Write((byte)CSPacketCode.Upgrade);
            msg.Write((byte)character);
            client.SendMessage(msg, NetDeliveryMethod.ReliableOrdered);
        }

        void onUpgrade(NetIncomingMessage msg)
        {
            int character = msg.ReadByte();
            simulation.SimulationInputQueue.Add(() =>
            {
                simulation.Upgrade(ResolveSender(msg), character);
            });
        }

        public void SetTarget(int character, bool isAllyTarget, int target)
        {
            var msg = client.CreateMessage();
            msg.Write((byte)CSPacketCode.SetTarget);
            msg.Write((byte)character);
            msg.Write((byte)(isAllyTarget ? 1 : 0));
            msg.Write((byte)target);
            client.SendMessage(msg, NetDeliveryMethod.ReliableOrdered);
        }

        void onSetTarget(NetIncomingMessage msg)
        {
            int player = ResolveSender(msg);
            int character = msg.ReadByte();
            bool isAllyTarget = msg.ReadByte() == 1;
            int targetedPlayer = player;
            if (!isAllyTarget)
                targetedPlayer = 1 - player;
            int target = msg.ReadByte();
            simulation.SimulationInputQueue.Add(() =>
            {
                simulation.SetTarget(player, character, targetedPlayer, target);
            });
        }

        public void UseSkill(int caster, int target)
        {
            var msg = client.CreateMessage();
            msg.Write((byte)CSPacketCode.UseSkill);
            msg.Write((byte)caster);
            msg.Write((byte)target);
            client.SendMessage(msg, NetDeliveryMethod.ReliableOrdered);
        }

        void onUseSkill(NetIncomingMessage msg)
        {
            int player = ResolveSender(msg);
            int caster = msg.ReadByte();
            int target = msg.ReadByte();
            simulation.SimulationInputQueue.Add(() =>
            {
                simulation.UseSkill(player, caster, target);
            });
        }

        #endregion

        #region Server --> Client

        public void ShowReadyButton()
        {
            Debug.Assert(server != null);

            var msg = server.CreateMessage();
            msg.Write((byte)SCPacketCode.ShowReadyButton);
            server.SendToAll(msg, NetDeliveryMethod.ReliableOrdered);

            for (int i = 0; i < Player.Max; i++)
            {
                ready[i] = false;
            }
        }

        void onShowReadyButton(NetIncomingMessage msg)
        {
            arenaScene.ShowReadyButton();
        }

        public void StartGame()
        {
            Debug.Assert(server != null);

            var msg = server.CreateMessage();
            msg.Write((byte)SCPacketCode.StartGame);
            server.SendToAll(msg, NetDeliveryMethod.ReliableOrdered);

            simulation = new SimulationImpl();
            simulation.Start();
        }

        void onStartGame(NetIncomingMessage msg)
        {
            arenaScene.StartGame();

            nextTickCount = 0;
            nextFrame = (Second)0;
        }

        public NetOutgoingMessage CreateGameTickMessage()
        {
            Debug.Assert(server != null);

            var msg = server.CreateMessage();
            msg.Write((byte)SCPacketCode.GameTick);

            return msg;
        }

        public void SendGameTickMessage(NetOutgoingMessage msg)
        {
            Debug.Assert(server != null);

            server.SendToAll(msg, NetDeliveryMethod.ReliableOrdered);
        }

        void onGameTickMessage(NetIncomingMessage msg)
        {
            int tick = msg.ReadInt32();
            QueueVoidCommand(msg, tick);
            while (msg.PositionInBytes != msg.LengthBytes)
            {
                var commandType = (SimulationCommand)msg.ReadByte();
                switch (commandType)
                {
                    case SimulationCommand.PlaceCharacter:
                        QueuePlaceCharacter(msg, tick);
                        break;
                    case SimulationCommand.Attack:
                        QueueAttack(msg, tick);
                        break;
                    case SimulationCommand.SetMana:
                        QueueSetMana(msg, tick);
                        break;
                    case SimulationCommand.RegenerateMana:
                        QueueRegenerateMana(msg, tick);
                        break;
                    case SimulationCommand.InstantCast:
                        QueueInstantCast(msg, tick);
                        break;
                    case SimulationCommand.BeginCast:
                        QueueBeginCast(msg, tick);
                        break;
                    case SimulationCommand.CancelCast:
                        QueueCancelCast(msg, tick);
                        break;
                    case SimulationCommand.EndCast:
                        QueueEndCast(msg, tick);
                        break;
                    case SimulationCommand.AttachEffect:
                        QueueAttachEffect(msg, tick);
                        break;
                    case SimulationCommand.DetachEffect:
                        QueueDetachEffect(msg, tick);
                        break;
                    case SimulationCommand.UpgradeBegin:
                        QueueUpgradeBegin(msg, tick);
                        break;
                    case SimulationCommand.UpgradeEnd:
                        QueueUpgradeEnd(msg, tick);
                        break;
                    case SimulationCommand.UpgradeCancelled:
                        QueueUpgradeCancelled(msg, tick);
                        break;
                    case SimulationCommand.Heal:
                        QueueHeal(msg, tick);
                        break;
                    case SimulationCommand.End:
                        return;
                }
            }
        }

        #endregion

        #region Simulation Command Queue

        Second nextFrame = (Second)0;
        int nextTickCount = 0;
        class QueuedCommand
        {
            public int Tick;
            public Action Command;
        }
        Utility.RecycleList<QueuedCommand> commandPool = new Utility.RecycleList<QueuedCommand>();
        List<QueuedCommand> toRemove = new List<QueuedCommand>();

        void ExecuteCommandQueue(GameTime gameTime)
        {
            nextFrame -= gameTime.DeltaTime;

            // 다음 프레임을 실행 할 수 있는 시간이면 실행
            if (nextFrame < 0)
            {
                int overFrame = 1;

                // 커맨드 풀에 다음 틱 정보가 있으면 실행.
                foreach (var cmd in commandPool)
                {
                    if (cmd.Tick == nextTickCount)
                    {
                        toRemove.Add(cmd);
                        cmd.Command();
                    }
                    if (cmd.Tick > nextTickCount)
                    {
                        overFrame = cmd.Tick - nextTickCount;
                    }
                }

                if (toRemove.Count > 0)
                {
                    foreach (var cmd in toRemove)
                    {
                        commandPool.Recycle(cmd);
                    }
                    toRemove.Clear();
                    nextFrame = (Second)(SimulationBase.LogicTick / (float)overFrame);
                    nextTickCount++;
                }
            }
        }

        void QueueVoidCommand(NetIncomingMessage msg, int tick)
        {
            var cmd = commandPool.NewItem();
            cmd.Command = () => { };
            cmd.Tick = tick;
        }

        void QueuePlaceCharacter(NetIncomingMessage msg, int tick)
        {
            int player = msg.ReadByte();
            int character = msg.ReadByte();
            int maxHp = msg.ReadInt16();


            var cmd = commandPool.NewItem();
            cmd.Command = () => { arenaScene.PlaceCharacter(player, character, maxHp); };
            cmd.Tick = tick;
        }

        void QueueSetMana(NetIncomingMessage msg, int tick)
        {
            int player = msg.ReadByte();
            int value = msg.ReadInt16();

            var cmd = commandPool.NewItem();
            cmd.Command = () => { arenaScene.SetMana(player, value); };
            cmd.Tick = tick;
        }

        void QueueRegenerateMana(NetIncomingMessage msg, int tick)
        {
            int player = msg.ReadByte();
            int regenValue = msg.ReadInt16();
            int manaAfter = msg.ReadInt16();

            var cmd = commandPool.NewItem();
            cmd.Command = () => { arenaScene.RegenerateMana(player, regenValue, manaAfter); };
            cmd.Tick = tick;
        }

        void QueueAttack(NetIncomingMessage msg, int tick)
        {
            int attackingPlayer = msg.ReadByte();
            int attacker = msg.ReadByte();
            int target = msg.ReadByte();
            int skill = msg.ReadByte();
            int damage = msg.ReadInt16();
            bool isMainTarget = msg.ReadByte() != 0;
            bool isCritical = msg.ReadByte() != 0;
            int hpAfter = msg.ReadInt16();

            var cmd = commandPool.NewItem();
            cmd.Command = () => { arenaScene.Attack(attackingPlayer, attacker, target, skill, damage, isMainTarget, isCritical, hpAfter); };
            cmd.Tick = tick;
        }

        void QueueInstantCast(NetIncomingMessage msg, int tick)
        {
            int player = msg.ReadByte();
            int caster = msg.ReadByte();
            int cost = msg.ReadInt16();
            int manaAfter = msg.ReadInt16();

            var cmd = commandPool.NewItem();
            cmd.Command = () => { arenaScene.InstantCast(player, caster, cost, manaAfter); };
            cmd.Tick = tick;
        }

        void QueueBeginCast(NetIncomingMessage msg, int tick)
        {
            int player = msg.ReadByte();
            int caster = msg.ReadByte();
            float time = msg.ReadFloat();
            int cost = msg.ReadInt16();
            int manaAfter = msg.ReadInt16();

            var cmd = commandPool.NewItem();
            cmd.Command = () => { arenaScene.BeginCast(player, caster, time, cost, manaAfter); };
            cmd.Tick = tick;
        }

        void QueueCancelCast(NetIncomingMessage msg, int tick)
        {
            int player = msg.ReadByte();
            int caster = msg.ReadByte();

            var cmd = commandPool.NewItem();
            cmd.Command = () => { arenaScene.CancelCast(player, caster); };
            cmd.Tick = tick;
        }

        void QueueEndCast(NetIncomingMessage msg, int tick)
        {
            int player = msg.ReadByte();
            int caster = msg.ReadByte();

            var cmd = commandPool.NewItem();
            cmd.Command = () => { arenaScene.EndCast(player, caster); };
            cmd.Tick = tick;
        }

        void QueueAttachEffect(NetIncomingMessage msg, int tick)
        {
            int player = msg.ReadByte();
            int target = msg.ReadByte();
            int effect = msg.ReadByte();
            int value = msg.ReadInt16();
            float lifeTime = msg.ReadFloat();

            var cmd = commandPool.NewItem();
            cmd.Command = () => { arenaScene.AttachEffect(player, target, effect, value, lifeTime); };
            cmd.Tick = tick;
        }

        void QueueDetachEffect(NetIncomingMessage msg, int tick)
        {
            int player = msg.ReadByte();
            int target = msg.ReadByte();
            int effect = msg.ReadByte();

            var cmd = commandPool.NewItem();
            cmd.Command = () => { arenaScene.DetachEffect(player, target, effect); };
            cmd.Tick = tick;
        }

        void QueueUpgradeBegin(NetIncomingMessage msg, int tick)
        {
            int player = msg.ReadByte();
            int target = msg.ReadByte();
            float time = msg.ReadFloat();
            int cost = msg.ReadInt16();
            int manaAfter = msg.ReadInt16();

            var cmd = commandPool.NewItem();
            cmd.Command = () => { arenaScene.UpgradeBegin(player, target, time, cost, manaAfter); };
            cmd.Tick = tick;
        }

        void QueueUpgradeEnd(NetIncomingMessage msg, int tick)
        {
            int player = msg.ReadByte();
            int target = msg.ReadByte();
            int levelAfter = msg.ReadByte();
            int hpAfter = msg.ReadInt16();
            int hpMaxAfter = msg.ReadInt16();

            var cmd = commandPool.NewItem();
            cmd.Command = () => { arenaScene.UpgradeEnd(player, target, levelAfter, hpAfter, hpMaxAfter); };
            cmd.Tick = tick;
        }

        void QueueUpgradeCancelled(NetIncomingMessage msg, int tick)
        {
            int player = msg.ReadByte();
            int target = msg.ReadByte();
            int refund = msg.ReadInt16();
            int manaAfter = msg.ReadInt16();

            var cmd = commandPool.NewItem();
            cmd.Command = () => { arenaScene.UpgradeCancelled(player, target, refund, manaAfter); };
            cmd.Tick = tick;
        }

        void QueueHeal(NetIncomingMessage msg, int tick)
        {
            int player = msg.ReadByte();
            int healer = msg.ReadByte();
            int target = msg.ReadByte();
            int skill = msg.ReadByte();
            int healAmount = msg.ReadInt16();
            bool isMainTarget = (msg.ReadByte() == 1);
            bool isCritical = (msg.ReadByte() == 1);
            int hpAfter = msg.ReadInt16();

            var cmd = commandPool.NewItem();
            cmd.Command = () => { arenaScene.Heal(player, healer, target, skill, healAmount, isMainTarget, isCritical, hpAfter); };
            cmd.Tick = tick;
        }

        #endregion

        int ResolveSender(NetIncomingMessage msg)
        {
            if (msg.SenderConnection == hostConnection)
            {
                return Player.Host;
            }
            else
            {
                return Player.Guest;
            }
        }

        NetServer server;
        NetClient client;
        NetConnection hostConnection;
        Scene.ArenaScene arenaScene;
        SystemComponent.LogSystem log;
        Scheduler scheduler = new Scheduler();
        SimulationImpl simulation;

        bool[] ready = new bool[Player.Max];
    }
}
