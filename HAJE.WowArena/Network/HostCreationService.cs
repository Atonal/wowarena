﻿using Lidgren.Network;
using System;
using System.Diagnostics;

namespace HAJE.WowArena.Network
{
    /// <summary>
    /// 서비스가 시작되면 서버를 만들고 UDP 홀펀칭을 위해 config에 적힌 대상에 주기적으로 날린다.
    /// 이 작업은 접속 상대가 나타나거나 강제 중단 될 때까지 반복된다.
    /// </summary>
    class HostCreationService : IUpdatable
    {
        public static HostCreationService RunningService
        {
            get;
            private set;
        }

        public event Action OnTimeOut = () => { };
        public event Action OnConnected = () => { };

        public NetServer Server { private set; get; }
        public NetClient Client { private set; get; }

        public void Start()
        {
            Debug.Assert(SessionCreationService.RunningService != null);
            Debug.Assert(HostCreationService.RunningService == null);
            Debug.Assert(Server == null);

            log = GameSystem.Log;
            log.WriteLine("호스트를 직접 엽니다. 게스트를 기다립니다.");

            HostCreationService.RunningService = this;

            owner = SessionCreationService.RunningService;
            owner.Scheduler.Register(this);
            config = owner.Configuration;

            // 서버 생성
            var netConfig = new NetPeerConfiguration(config.appName);
            netConfig.AutoFlushSendQueue = true;
            netConfig.EnableMessageType(NetIncomingMessageType.DebugMessage);
            netConfig.EnableMessageType(NetIncomingMessageType.VerboseDebugMessage);
            netConfig.EnableMessageType(NetIncomingMessageType.DiscoveryRequest);
            netConfig.EnableMessageType(NetIncomingMessageType.ConnectionApproval);
            netConfig.Port = config.port;
            Server = new NetServer(netConfig);
            Server.Start();

            // 홀펀칭을 위한 UDP 패킷 전송 예약
            holePunchingWorker = scheduler.ScheduleRepeat(SendHolePunchingMessage, (Second)3.0f);
        }

        public void Abort()
        {
            Debug.Assert(HostCreationService.RunningService == this);
            Debug.Assert(Server != null);

            HostCreationService.RunningService = null;

            Server.Shutdown("HostCreationService.Abort()");
            Server = null;
            owner.Scheduler.Hide(this);
        }

        public void End()
        {
            Debug.Assert(HostCreationService.RunningService == this);
            Debug.Assert(Server != null);

            HostCreationService.RunningService = null;

            owner.Scheduler.Hide(this);
        }

        public void Update(GameTime gameTime)
        {
            scheduler.Update(gameTime);

            if (Server != null)
            {
                NetIncomingMessage msg;
                while ((msg = Server.ReadMessage()) != null)
                {
                    switch (msg.MessageType)
                    {
                        case NetIncomingMessageType.DebugMessage:
                        case NetIncomingMessageType.ErrorMessage:
                        case NetIncomingMessageType.WarningMessage:
                        case NetIncomingMessageType.VerboseDebugMessage:
                            log.WriteLine("처리되지 않은 서버 메세지: " + msg.ReadString());
                            break;
                        case NetIncomingMessageType.StatusChanged:
                            NetConnectionStatus status = (NetConnectionStatus)msg.ReadByte();
                            string reason = msg.ReadString();
                            log.WriteLine("서버 상태 변경: " + reason);
                            if (Server.ConnectionsCount == 2)
                            {
                                OnConnected();
                            }
                            break;
                        case NetIncomingMessageType.DiscoveryRequest:
                            log.WriteLine("호스트 탐색 요청 수신: " + msg.SenderEndPoint);
                            OnDiscoveryRequest(msg.SenderEndPoint);
                            break;
                        case NetIncomingMessageType.ConnectionApproval:
                            log.WriteLine("접속 요청: " + msg.SenderEndPoint);
                            if (Server.ConnectionsCount == 0)
                            {
                                msg.SenderConnection.Approve();
                                log.WriteLine("접속 요청 수락 됨");

                                var netConfig = new NetPeerConfiguration(config.appName);
                                netConfig.AutoFlushSendQueue = true;
                                netConfig.EnableMessageType(NetIncomingMessageType.DebugMessage);
                                netConfig.EnableMessageType(NetIncomingMessageType.VerboseDebugMessage);
                                netConfig.EnableMessageType(NetIncomingMessageType.DiscoveryResponse);
                                Client = new NetClient(netConfig);
                                Client.Start();
                                Client.Connect("localHost", config.port);
                            }
                            else if (msg.SenderEndPoint.Address.ToString() == "127.0.0.1"
                                && Server.ConnectionsCount == 1)
                            {
                                msg.SenderConnection.Approve();
                                log.WriteLine("접속 요청 수락 됨");
                            }
                            else
                            {
                                msg.SenderConnection.Deny();
                                log.WriteLine("접속 요청 거절 됨");
                            }
                            break;
                        case NetIncomingMessageType.Data:
                            log.WriteLine("서버 데이터 수신 됨");
                            break;
                    }
                    Server.Recycle(msg);
                }
            }
        }

        void SendHolePunchingMessage()
        {
            log.WriteLine("접속 후보자들에게 홀펀칭 패킷 전송");
            foreach (var endPoint in config.ConnectionCandidates)
            {
                var msg = Server.CreateMessage();
                msg.Write("Hole Punching Message");
                Server.SendUnconnectedMessage(msg, endPoint.ip, endPoint.port);
            }
        }

        void OnDiscoveryRequest(System.Net.IPEndPoint endPoint)
        {
            if (Server.ConnectionsCount == 0)
            {
                var msg = Server.CreateMessage();
                msg.Write("Available Host");
                Server.SendDiscoveryResponse(msg, endPoint);
                log.WriteLine("호스트 탐색 요청 수락: " + endPoint);
            }
            else
            {
                log.WriteLine("호스트 탐색 요청 거절: " + endPoint);
            }
        }

        SystemComponent.LogSystem log;
        SessionCreationService owner;
        Configuration config;
        Scheduler scheduler = new Scheduler();
        ScheduledFunction holePunchingWorker;
    }
}
