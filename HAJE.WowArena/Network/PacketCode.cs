﻿
namespace HAJE.WowArena.Network
{
    /// <summary>
    /// Client To Server Packet Code
    /// </summary>
    enum CSPacketCode
    {
        SetReady = 0,
        Upgrade,
        SetTarget,
        UseSkill,
    }

    /// <summary>
    /// Server To Client Packet Code
    /// </summary>
    enum SCPacketCode
    {
        ShowReadyButton = 0,
        StartGame,
        GameTick,
    }
}
