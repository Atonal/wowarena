﻿using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace HAJE.WowArena.Network
{
    /// <summary>
    /// config를 읽어서 게임 세션을 만드는 처리
    /// 그에 필요한 장면 전환등의 처리고 함께 다른다.
    /// </summary>
    class SessionCreationService : IUpdatable
    {
        public static SessionCreationService RunningService
        {
            get;
            private set;
        }

        public void Start()
        {
            Debug.Assert(RunningService == null);
            RunningService = this;

            Scheduler = new WowArena.Scheduler();
            GameSystem.UpdateLoop.Register(this);

            Configuration = Configuration.Load();
            log = GameSystem.Log;
            log.WriteLine("세션 생성을 시작합니다.");
            log.WriteLine("Configuration -------------------------------------------");
            log.WriteLine("> connectionType = " + Configuration.connectionType);
            log.WriteLine("> port = " + Configuration.port);
            log.WriteLine("> connection address list");
            if (Configuration.ConnectionCandidates.Length == 0)
            {
                log.WriteLine(">   empty");
            }
            foreach (var addr in Configuration.ConnectionCandidates)
            {
                log.WriteLine(">   " + addr.ip + ":" + addr.port);
            }
            log.WriteLine("---------------------------------------------------------");
            connectionScene = new Scene.ConnectionScene();
            connectionScene.Start();

            switch (Configuration.connectionType)
            {
                case Configuration.ConnectionType.auto:
                    discoverService = new HostDiscoveryService();
                    discoverService.OnTimeOut += RetryDiscoveryOrCreateHost;
                    discoverService.OnDiscovered += OnDiscoveredHandler;
                    discoverService.Start();
                    break;
                case Configuration.ConnectionType.forceGuest:
                    discoverService = new HostDiscoveryService();
                    discoverService.OnTimeOut += discoverService.Retry;
                    discoverService.OnDiscovered += OnDiscoveredHandler;
                    discoverService.Start();
                    break;
                case Configuration.ConnectionType.forceHost:
                    CreateHost();
                    break;
                case WowArena.Configuration.ConnectionType.localTest:
                    try
                    {
                        CreateHost();
                    }
                    catch
                    {
                        hostCreationService.Abort();
                        hostCreationService = null;

                        discoverService = new HostDiscoveryService();
                        discoverService.OnTimeOut += discoverService.Retry;
                        discoverService.OnDiscovered += OnDiscoveredHandler;
                        discoverService.Start();
                    }
                    break;
                default:
                    log.WriteLine("아직 구현되지 않은 connection type입니다: " + Configuration.connectionType);
                    break;
            }
        }

        public void End()
        {
            Debug.Assert(RunningService == this);
            RunningService = null;

            connectionScene.End();
            GameSystem.UpdateLoop.Hide(this);
        }

        public void Update(GameTime gameTime)
        {
            Scheduler.Update(gameTime);
        }

        public Configuration Configuration { get; private set; }
        public Scheduler Scheduler { get; private set; }

        void RetryDiscoveryOrCreateHost()
        {
            if (rand.NextDouble() < 0.5)
            {
                discoverService.Retry();
                discoverService.OnTimeOut -= RetryDiscoveryOrCreateHost;
                discoverService.OnTimeOut += CreateHost;
            }
            else
            {
                try
                {
                    CreateHost();
                }
                catch (Exception e)
                {
                    // 이미 로컬 호스트가 있는 등의 이유로 호스트 생성에 실패한 경우
                    log.WriteLine(e.Message);
                    hostCreationService.Abort();
                    hostCreationService = null;
                    RestartDiscovery();
                }
            }
        }

        void RestartDiscovery()
        {
            if (discoverService == null)
            {
                discoverService = new HostDiscoveryService();
                discoverService.Start();
                if (Configuration.connectionType == WowArena.Configuration.ConnectionType.auto)
                {
                    discoverService.OnTimeOut += RetryDiscoveryOrCreateHost;
                }
                else
                {
                    discoverService.OnTimeOut += discoverService.Retry;
                }
                discoverService.OnDiscovered += OnDiscoveredHandler;
            }
            else
            {
                discoverService.Retry();
            }
        }

        void OnDiscoveredHandler()
        {
            var ip = discoverService.DiscoveredIp;
            var port = discoverService.DiscoveredPort;
            var client = discoverService.Client;
            discoverService.End();
            discoverService = null;

            client.Connect(ip, port);
            OnSessionCreated(null, client);
        }

        void CreateHost()
        {
            if (discoverService != null)
            {
                discoverService.Abort();
                discoverService = null;
            }

            hostCreationService = new HostCreationService();
            hostCreationService.Start();
            hostCreationService.OnConnected += OnConnectedHandler;
        }

        void OnConnectedHandler()
        {
            var server = hostCreationService.Server;
            var client = hostCreationService.Client;

            hostCreationService.End();

            OnSessionCreated(server, client);
        }

        void OnSessionCreated(NetServer server, NetClient client)
        {
            log.WriteLine("세션이 생성되었습니다.");
            new Session().Start(server, client);
            End();
        }

        SystemComponent.LogSystem log;
        Scene.ConnectionScene connectionScene;
        HostDiscoveryService discoverService;
        HostCreationService hostCreationService;
        Random rand = new Random();
    }
}
