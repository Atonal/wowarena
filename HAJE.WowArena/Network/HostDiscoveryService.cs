﻿using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace HAJE.WowArena.Network
{
    /// <summary>
    /// 서비스가 시작되면 config에 적힌 대상 주소와 로컬에 있는 호스트를 탐색한다.
    /// 임의의 시간이 지나면 TimeOut 되어 더 이상 호스트를 탐색하지 않는다.
    /// 호스트가 찾아지거나, 
    /// </summary>
    class HostDiscoveryService : IUpdatable
    {
        public static HostDiscoveryService RunningService
        {
            get;
            private set;
        }

        public event Action OnTimeOut = () => { };
        public event Action OnDiscovered = () => { };

        public string DiscoveredIp { get; private set; }
        public int DiscoveredPort { get; private set; }
        public NetClient Client { get; private set; }

        public void Start()
        {
            Debug.Assert(SessionCreationService.RunningService != null);
            Debug.Assert(HostDiscoveryService.RunningService == null);
            Debug.Assert(Client == null);
            Debug.Assert(discoverTrials.Count == 0);

            log = GameSystem.Log;
            log.WriteLine("호스트 탐색을 시작합니다.");

            HostDiscoveryService.RunningService = this;

            owner = SessionCreationService.RunningService;
            owner.Scheduler.Register(this);
            config = owner.Configuration;

            // 클라이언트 생성
            var netConfig = new NetPeerConfiguration(config.appName);
            netConfig.AutoFlushSendQueue = true;
            netConfig.EnableMessageType(NetIncomingMessageType.DebugMessage);
            netConfig.EnableMessageType(NetIncomingMessageType.VerboseDebugMessage);
            netConfig.EnableMessageType(NetIncomingMessageType.DiscoveryResponse);
            Client = new NetClient(netConfig);
            Client.Start();

            SendDiscovery();
        }

        public void Retry()
        {
            Debug.Assert(Client != null);

            UnschduleAll();
            log.WriteLine("호스트 탐색을 다시 시도합니다.");
            SendDiscovery();
        }

        public void Abort()
        {
            log.WriteLine("호스트 탐색을 중지 합니다.");
            UnschduleAll();
            if (Client != null)
            {
                Client.Shutdown("abort");
                Client = null;
            }
            End();
        }

        public void End()
        {
            Debug.Assert(RunningService == this);

            log.WriteLine("호스트 탐색 종료.");
            owner.Scheduler.Hide(this);
            RunningService = null;
        }

        public void Update(GameTime gameTime)
        {
            scheduler.Update(gameTime);

            if (Client != null)
            {
                NetIncomingMessage msg;
                while ((msg = Client.ReadMessage()) != null)
                {
                    switch (msg.MessageType)
                    {
                        case NetIncomingMessageType.DebugMessage:
                        case NetIncomingMessageType.ErrorMessage:
                        case NetIncomingMessageType.WarningMessage:
                        case NetIncomingMessageType.VerboseDebugMessage:
                            log.WriteLine("처리되지 않은 클라 메세지: " + msg.ReadString());
                            break;
                        case NetIncomingMessageType.StatusChanged:
                            NetConnectionStatus status = (NetConnectionStatus)msg.ReadByte();
                            string reason = msg.ReadString();
                            log.WriteLine("클라 상태 변경: " + reason);
                            break;
                        case NetIncomingMessageType.DiscoveryResponse:
                            if (RunningService == this)
                            {
                                log.WriteLine("호스트 발견 됨: " + msg.SenderEndPoint);
                                DiscoveredIp = msg.SenderEndPoint.Address.ToString();
                                DiscoveredPort = msg.SenderEndPoint.Port;
                                OnDiscovered();
                            }
                            else
                            {
                                log.WriteLine("호스트가 발견 되었으나 무시합니다: " + msg.SenderEndPoint);
                            }
                            break;
                        case NetIncomingMessageType.Data:
                            log.WriteLine("클라 데이터 수신 됨");
                            break;
                    }
                    Client.Recycle(msg);
                }
            }
        }

        void SendDiscovery()
        {
            Debug.Assert(discoveryTimeOutHandler == null);
            Debug.Assert(discoverTrials.Count == 0);

            Second trialTime = (Second)rand.NextFloat(5.0f, 15.0f);
            int maxTrial = Math.Max(3, (int)(trialTime / rand.NextFloat(2.0f, 4.0f)));
            for (int trial = 1; trial <= maxTrial; trial++)
            {
                var trialCapture = trial;
                var scheduled = scheduler.ScheduleOnce(() =>
                {
                    log.WriteLine("호스트 탐색 메세지 전송 중 ({0}/{1})", trialCapture, maxTrial);
                    if (config.connectionType != Configuration.ConnectionType.localTest)
                    {
                        Client.DiscoverLocalPeers(config.port);
                        foreach (var host in config.ConnectionCandidates)
                        {
                            Client.DiscoverKnownPeer(host.ip, host.port);
                        }
                    }
                    else
                    {
                        Client.DiscoverKnownPeer("localHost", config.port);
                    }

                }, trialTime * ((trial - 1.0f) / (float)maxTrial));
                discoverTrials.Add(scheduled);
            }

            discoveryTimeOutHandler = scheduler.ScheduleOnce(() =>
            {
                UnschduleAll();
                log.WriteLine("호스트를 찾지 못했습니다.");

                OnTimeOut();
            },
            trialTime);
        }

        void UnschduleAll()
        {
            foreach (var trials in discoverTrials)
            {
                scheduler.Unschedule(trials);
            }
            discoverTrials.Clear();
            discoveryTimeOutHandler = null;
        }
        
        SystemComponent.LogSystem log;
        SessionCreationService owner;
        Configuration config;
        Scheduler scheduler = new Scheduler();
        Random rand = new Random();
        List<ScheduledFunction> discoverTrials = new List<ScheduledFunction>();
        ScheduledFunction discoveryTimeOutHandler = null;
    }
}
