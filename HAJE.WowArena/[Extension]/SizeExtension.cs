﻿using SharpDX;
using System;
using System.Drawing;

namespace HAJE.WowArena
{
    public static class SizeExtension
    {
        public static Vector2 ToVector2(this Size size)
        {
            return new Vector2(size.Width, size.Height);
        }
    }
}
