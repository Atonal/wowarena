﻿using SharpDX;
using System;

namespace HAJE.WowArena
{
    public static class Vector2Extension
    {
        public static Size2 ToSize2(this Vector2 vec)
        {
            return new Size2((int)Math.Ceiling(vec.X), (int)Math.Ceiling(vec.Y));
        }
    }
}
