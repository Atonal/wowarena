﻿using SharpDX;
using System;

namespace HAJE.WowArena
{
    public static class Vector4Extension
    {
        public static Vector3 XYZ(this Vector4 vec)
        {
            return new Vector3(vec.X, vec.Y, vec.Z);
        }
    }
}
