﻿using System.Collections.Generic;

namespace HAJE.WowArena.Utility
{
    public class RecycleList<T> : IEnumerable<T>
        where T : new()
    {
        public T NewItem()
        {
            T e;
            if (garbage.Count == 0)
                e = new T();
            else
                e = garbage.Dequeue();
            list.Add(e);
            return e;
        }

        public void Clear()
        {
            foreach (var e in list)
                garbage.Enqueue(e);
            list.Clear();
        }

        public void Recycle(T e)
        {
            list.Remove(e);
            garbage.Enqueue(e);
        }

        public int Count
        {
            get
            {
                return list.Count;
            }
        }

        List<T> list = new List<T>();
        Queue<T> garbage = new Queue<T>();

        #region IEnumerable 멤버

        public IEnumerator<T> GetEnumerator()
        {
            return list.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return (list as System.Collections.IEnumerable).GetEnumerator();
        }

        #endregion
    }
}
