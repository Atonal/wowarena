﻿using SharpDX;
using System.Collections.Generic;
using System.Diagnostics;

namespace HAJE.WowArena.SystemComponent
{
    public class LogSystem
    {
        public void WriteLine(string format, params object[] args)
        {
            const int maxLine = 150;
            string text = string.Format(format, args);

            if (Debugger.IsAttached)
                Debug.WriteLine(text);

            Log.Enqueue(text);
            while (Log.Count > maxLine)
                Log.Dequeue();
        }

        public Queue<string> Log = new Queue<string>();
    }
}
