﻿using HAJE.WowArena.Input;
using SharpDX;
using SharpDX.Direct3D9;
using System;
using System.Collections;
using System.Collections.Generic;

namespace HAJE.WowArena.SystemComponent
{
    public class DebugOutput : IDisposable
    {
        public DebugOutput(Device device, Keyboard keyboard)
        {
            var fontDesc = new FontDescription()
            {
                CharacterSet = FontCharacterSet.Ansi | FontCharacterSet.Hangul,
                FaceName = "굴림체",
                Height = 14,
                Italic = false,
                Weight = FontWeight.Bold,
                MipLevels = 0,
                OutputPrecision = FontPrecision.TrueType,
                PitchAndFamily = FontPitchAndFamily.Default,
                Quality = FontQuality.ClearType
            };
            font = new Font(device, fontDesc);

            if (GameSystem.BuildInfo.DebugBuild || System.Diagnostics.Debugger.IsAttached)
            {
                keyboard.KeyUp += (k) =>
                    {
                        if (k == Keys.F12)
                        {
                            visible = !visible;
                        }
                    };
            }

            visible = GameSystem.BuildInfo.DebugBuild;
        }

        public void WriteText(string text, int x, int y, Color color)
        {
            Label label = drawList.NewItem();
            label.X = x;
            label.Y = y;
            label.Color = new ColorBGRA(color.ToBgra());
            label.Text = text;
        }

        public void SetWatch(string label, object text)
        {
            SetWatch(label, text, Color.Gray);
        }

        public void SetWatch(string label, object text, Color color)
        {
            if (watchList.ContainsKey(label))
            {
                watchList[label].Text = text.ToString();
                watchList[label].Color = color;
            }
            else
            {
                watchList.Add(label, new Watch()
                {
                    Color = color,
                    Text = text.ToString(),
                    Label = label
                });
            }
        }

        public void Draw()
        {
            if (visible) DrawDefault();

            foreach (var l in drawList)
            {
                font.DrawText(null, l.Text, l.X, l.Y, l.Color);
            }
            drawList.Clear();
        }

        public void Dispose()
        {
            if (font != null) font.Dispose(); font = null;
        }

        private Font font;
        private bool visible = true;

        private void WriteImmidiate(string text, int x, int y, Color color)
        {
            font.DrawText(null, text, x, y, new ColorBGRA(color.ToBgra()));
        }

        private void DrawDefault()
        {
            const int topBase = 5;
            const int leftBase = 5;
            const int lineHeight = 15;
            int top = topBase;
            int left = leftBase;
            
            // fps출력
            float fps = GameSystem.Profiler.Fps.Average;
            Color fpsColor = Color.White;
            if (fps >= 59) fpsColor = Color.Lime;
            else if (fps >= 40) fpsColor = Color.YellowGreen;
            else if (fps >= 24) fpsColor = Color.Yellow;
            else if (fps >= 15) fpsColor = Color.Orange;
            else fpsColor = Color.Red;

            WriteImmidiate(string.Format("fps: {0,-6}  ({1,-6}-{2,6})",
                fps.ToString("F1"),
                GameSystem.Profiler.Fps.Minimum.ToString("F1"),
                GameSystem.Profiler.Fps.Maximum.ToString("F1")),
                left, top, fpsColor);

            // 프로파일러 탐침 출력
            top += lineHeight * 3;
            foreach(var probe in GameSystem.Profiler.ProbeProfiles)
            {
                float timeInMs = (probe.Value * 1000.0f);

                int roundDigit = 0;
                if (timeInMs < 0.1f) roundDigit = 3;
                else if (timeInMs < 1) roundDigit = 2;
                else if (timeInMs < 10) roundDigit = 1;
                else roundDigit = 0;
                string msFormat = "F" + roundDigit;
                timeInMs = (float)Math.Round(timeInMs, roundDigit);

                Color probeColor = Color.White;
                if (timeInMs < 1) probeColor = Color.Gray;
                else if (timeInMs < 2) probeColor = Color.White;
                else if (timeInMs <= 5) probeColor = Color.Yellow;
                else if (timeInMs <= 10) probeColor = Color.Orange;
                else probeColor = Color.Red;

                WriteImmidiate(probe.Key + ": " + timeInMs.ToString(msFormat) + " ms", left, top, probeColor);
                top += lineHeight;
            }
            WriteImmidiate("--------------------------------", left, top, Color.DarkRed);
            top += lineHeight;

            float frameInterval = 1000.0f / GameSystem.Profiler.Fps.Average;
            WriteImmidiate("frame interval: " + frameInterval.ToString("F1") + " ms", left, top, Color.DarkRed);
            top += lineHeight;

            // Watch 값 출력
            top += lineHeight * 2;
            foreach (var w in watchList.Values)
            {
                WriteImmidiate(w.Label + ": " + w.Text, left, top, w.Color);
                top += lineHeight;
            }

            // 로그 출력
            left = 300;
            top = topBase + lineHeight * 3;
            int viewHeight = GameSystem.GraphicsDevice.Viewport.Height;
            int maxLogs = (viewHeight - top) / lineHeight;
            int logFrom = Math.Max(0, GameSystem.Log.Log.Count - maxLogs);
            int logIndex = 0;
            foreach (string log in GameSystem.Log.Log)
            {
                if (logIndex >= logFrom)
                {
                    WriteImmidiate(log, left, top, Color.Gray);
                    top += lineHeight;
                }
                logIndex++;
            }
        }

        class Label
        {
            public string Text;
            public int X;
            public int Y;
            public ColorBGRA Color;
        }
        Utility.RecycleList<Label> drawList = new Utility.RecycleList<Label>();

        class Watch
        {
            public string Label;
            public string Text;
            public Color Color;
        }
        Dictionary<string, Watch> watchList = new Dictionary<string, Watch>();
    }
}
