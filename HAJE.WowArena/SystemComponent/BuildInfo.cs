﻿
namespace HAJE.WowArena.SystemComponent
{
    public class BuildInfo
    {
#if DEBUG
        public readonly bool DebugBuild = true;
#else
        public readonly bool DebugBuild = false;
#endif
    }
}
