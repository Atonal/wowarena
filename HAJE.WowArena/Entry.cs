﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HAJE.WowArena
{
    static class Entry
    {
        static void Main()
        {
            GameSystem.Initialize();
            new Network.SessionCreationService().Start();
            //new Scene.ArenaScene().Start();
            GameSystem.Run();
            if (Network.Session.RunningSession != null)
            {
                Network.Session.RunningSession.End();
            }
            GameSystem.FinalizeSystem();
        }
    }
}
