﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace HAJE.WowArena
{
    public class ScheduledFunction : IUpdatable
    {
        /// <summary>
        /// 스케쥴러 이외의 곳에서 직접 생성 금지.
        /// </summary>
        public ScheduledFunction(Action function, Scheduler owner)
        {
            Debug.Assert(owner != null);
            Function = function;
            Owner = owner;
            Loop = false;
            Alive = false;
        }

        public void ScheduleRepeat(Second interval)
        {
            Debug.Assert(!Alive);
            Debug.Assert(interval > 0);
            Loop = true;
            Alive = true;
            delay = this.interval = interval;
        }

        public void ScheduleOnce(Second delay)
        {
            Debug.Assert(!Alive);
            Loop = false;
            Alive = true;
            this.delay = interval = delay;
        }

        public void Unschedule()
        {
            Alive = false;
        }

        public void Update(GameTime gameTime)
        {
            if (Alive)
            {
                delay -= gameTime.DeltaTime;
                if (delay < 0)
                {
                    if (Loop)
                    {
                        while (delay < 0)
                        {
                            if (Alive)
                            {
                                Function();
                                delay += interval;
                            }
                        }
                    }
                    else
                    {
                        Function();
                        Alive = false;
                    }
                }
            }
        }

        public readonly Action Function;
        public readonly Scheduler Owner;
        public bool Loop { get; private set; }
        public bool Alive { get; private set; }

        private Second delay = (Second)0;
        private Second interval = (Second)0;
    }

    public class Scheduler : UpdateDispatcher
    {
        public ScheduledFunction ScheduleRepeat(Action function, Second interval)
        {
            Debug.Assert(interval > 0);
            var ret = new ScheduledFunction(function, this);
            ret.ScheduleRepeat(interval);
            tobeAdded.Add(ret);
            return ret;
        }

        public ScheduledFunction ScheduleOnce(Action function, Second delay)
        {
            var ret = new ScheduledFunction(function, this);
            ret.ScheduleOnce(delay);
            tobeAdded.Add(ret);
            return ret;
        }

        public void Unschedule(ScheduledFunction function)
        {
            function.Unschedule();
        }

        public override void Update(GameTime gameTime)
        {
            foreach (var e in tobeAdded)
            {
                updateList.Add(e);
            }
            tobeAdded.Clear();

            foreach (var e in updateList)
            {
                if (e.Alive) e.Update(gameTime);
                if (!e.Alive) tobeRemoved.Add(e);
            }

            foreach (var e in tobeRemoved)
            {
                updateList.Remove(e);
            }
            tobeRemoved.Clear();

            base.Update(gameTime);
        }

        List<ScheduledFunction> updateList = new List<ScheduledFunction>();
        List<ScheduledFunction> tobeAdded = new List<ScheduledFunction>();
        List<ScheduledFunction> tobeRemoved = new List<ScheduledFunction>();
    }
}
