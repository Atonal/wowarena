﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HAJE.WowArena.Simulation
{
    class Character
    {
        public const int Hunter = 0;
        public const int Warrior = 1;
        public const int Mage = 2;
        public const int Paladin = 3;
        public const int Priest = 4;
        public const int Max = 5;
        public const int levelMax = 5;

        public static readonly string[] className = {"hunter", "warrior", "mage", "paladin", "priest"};

        public int id = -1;
        public int level = 1;

        public int hp = 100;

        public int MainTarget = 0;

        public ScheduledFunction LastAction;
        public ScheduledFunction LastUpgrade;
        public List<Effect> EffectList = new List<Effect>();
        public float SkillCooldown = 0.0f;
        public float AttackCooldown = 3.0f;
        public float AttackCooldownConst = 3.0f;
        public int CritPercentage = 0;
        public float DamageDoneAmplify = 1.0f;
        public float DamageTakenAmplify = 1.0f;

        public delegate void BasicAttackHandler(int player, int caster, int target);
        public BasicAttackHandler BasicAttack;
    }
}
