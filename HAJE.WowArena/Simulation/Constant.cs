﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HAJE.WowArena.Simulation
{
    public static class BalanceConstant
    {
        /// <summary>
        /// 전역 상수
        /// </summary>
        public class GlobalConstant
        {
            public readonly int initialMana = 0;
            public readonly int MaxUpgrade = 5;
            public readonly float CriticalChance = 0.00f;
            public readonly float CriticalDamage = 2.00f;
            public readonly float ProjectileLifeTime = 1.5f;

            public readonly float DamageVariation = 0.00f;

            public readonly float[] UpgradeCost = { 0, 100, 150, 200, 300 };
            public readonly float[] UpgradeTime = { 0, 15, 20, 23, 25, 30 };
            public readonly float[] ManaRegen = { 0, 2.0f, 3.0f, 3.5f, 3.75f, 4.0f };
        }
        public static readonly GlobalConstant Global = new GlobalConstant();

        /// <summary>
        /// 사냥꾼 관련 상수
        /// </summary>
        public class HunterConstant
        {
            public readonly int[] MaxHp = { 0, 300, 350, 400, 500, 600 };

            /// <summary>
            /// 사냥꾼 평타
            /// </summary>
            public class AttackConstant
            {
                public readonly float Cooldown = 2.0f;
                public readonly float[] Damage = { 0, 10, 12, 14, 16, 18 };
            }
            public AttackConstant Attack = new AttackConstant();

            /// <summary>
            /// 사냥꾼 조준 사격
            /// </summary>
            public class AimedShotConstant
            {
                public readonly int Cost = 10;
                public readonly float Casting = 3.0f;
                public readonly float Cooldown = 3.0f;
                public readonly float Damage = 30.0f;
                public readonly float RemainTime = 15.0f;
                public readonly float AdditionalCriticalDamage = 1.00f;
                public readonly int MaxStack = 3;
                public readonly float[] StackDamageBonus = { 0, 20, 22, 25, 30, 40 };
            }
            public AimedShotConstant AimedShot = new AimedShotConstant();

            /// <summary>
            /// 사냥꾼 집중
            /// </summary>
            public class ConcentrationConstant
            {
                public readonly int MaxStack = 5;
                public readonly float[] StackDamageBonus = { 0, 15, 16, 17, 18, 20 };
                public readonly float RemainTime = 6.0f;
            }
            public ConcentrationConstant Concentration = new ConcentrationConstant();
        }
        public static readonly HunterConstant Hunter = new HunterConstant();
        
        /// <summary>
        /// 전사 관련 상수
        /// </summary>
        public class WarriorConstant
        {
            public readonly int[] MaxHp = { 0, 400, 475, 550, 625, 700 };

            /// <summary>
            /// 전사 평타
            /// </summary>
            public class AttackConstant
            {
                public readonly float Cooldown = 4.0f;
                public readonly float[] Damage = { 0, 4, 6, 8, 10, 12 };
            }
            public AttackConstant Attack = new AttackConstant();

            /// <summary>
            /// 전사 가로막기
            /// </summary>
            public class InterveneConstant
            {
                public readonly int Cost = 20;
                public readonly float Casting = 0.0f;
                public readonly float Cooldown = 12.0f ;
                public readonly float[] RemainTime = { 0, 5.0f, 5.0f, 6.0f, 7.0f, 8.0f };
                public readonly float DamageReduce = 0.7f;
            }
            public InterveneConstant Intervene = new InterveneConstant();

            /// <summary>
            /// 전사 
            /// </summary>
            public class VengenceConstant
            {
                public readonly float StackDamageBonus = 0.3f;
                public readonly float RemainTime = 6f;
            }
            public VengenceConstant Vengence = new VengenceConstant();
        }
        public static readonly WarriorConstant Warrior = new WarriorConstant();
        
        /// <summary>
        /// 마법사 관련 상수
        /// </summary>
        public class MagicianConstant
        {
            public readonly int[] MaxHp = { 0, 250, 300, 350, 400, 450 };

            /// <summary>
            /// 마법사 평타
            /// </summary>
            public class AttackConstant
            {
                public readonly int Cooldown = 4;
                public readonly float[] Damage = { 0, 5, 6, 7, 8, 9 };
            }
            public AttackConstant Attack = new AttackConstant();

            /// <summary>
            /// 마법사 동결
            /// </summary>
            public class DeepFreezeConstant
            {
                public readonly int Cost = 40;
                public readonly float Casting = 0.0f;
                public readonly float[] RemainTime = { 0, 5.0f, 5.5f, 6.0f, 6.5f, 7.0f };
                public readonly float Cooldown = 40.0f;
            }
            public DeepFreezeConstant DeepFreeze = new DeepFreezeConstant();

            /// <summary>
            /// 마법사 마법 훔치기
            /// </summary>
            public class DevourMagicConstant
            {
                public readonly float ReductionTime = 5.0f;
             }
            public DevourMagicConstant DevourMagic = new DevourMagicConstant();
        }
        public static readonly MagicianConstant Magician = new MagicianConstant();
        
        /// <summary>
        /// 성기사 관련 상수
        /// </summary>
        public class PaladinConstant
        {
            public readonly int[] MaxHp = { 0, 400, 450, 500, 550, 600 };

            /// <summary>
            /// 성기사 평타
            /// </summary>
            public class AttackConstant
            {
                public readonly float Cooldown = 3.0f;
                public readonly float[] Damage = { 0, 5.0f, 5.5f, 6.0f, 6.5f, 7.0f};
                public readonly float[] SplashPercent = { 0.2f, 0.4f, 0.6f, 0.8f, 1.0f };
            }
            public AttackConstant Attack = new AttackConstant();

            /// <summary>
            /// 신의 가호
            /// </summary>
            public class DivineProtectionConstant
            {
                public readonly int Cost = 50;
                public readonly float Casting = 2.0f;
                public readonly float[] RemainTime = { 0, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f };
                public readonly float DamageReduction = 0.5f ;
                public readonly float DamageAmplify = 1.5f;
                public readonly float Cooldown = 120.0f;
            }
            public DivineProtectionConstant DivineProtection = new DivineProtectionConstant();

            /// <summary>
            /// 관대한 치유사
            /// </summary>
            public class SelflessHealerConstant
            {
                public readonly float AllyFactor = -2.0f;
             }
            public SelflessHealerConstant Concentration = new SelflessHealerConstant();
        }
        public static readonly PaladinConstant Paladin = new PaladinConstant();
        
        /// <summary>
        /// 사제 관련 상수
        /// </summary>
        public class PriestConstant
        {
            public readonly int[] MaxHp = { 0,250, 300, 350, 400, 450 };

            /// <summary>
            /// 사제 평타
            /// </summary>
            public class AttackConstant
            {
                public readonly float Cooldown = 3.0f;
                public readonly float[] Heal = { 0, 10.0f, 12.0f, 14.0f, 16.0f, 18.0f};
            }
            public AttackConstant Attack = new AttackConstant();

            /// <summary>
            /// 소생
            /// </summary>
            public class RenewConstant
            {
                public readonly int Cost = 30;
                public readonly float Casting = 3f;
                public readonly float[] TickHeal = { 0, 10.0f, 11.0f, 12.0f, 13.0f, 14.0f };
                public readonly float TickTime = 2.0f;
                public readonly int TickNum = 5;
                public readonly float RemainTime = 10.0f;
                public readonly float Cooldown = 8.0f;
            }
            public RenewConstant Renew = new RenewConstant();

            /// <summary>
            /// 빛의 반향
            /// </summary>
            public class EchoOfLightConstant
            {
                public readonly float TickTime = 2.0f;
                public readonly float RemainTime = 6.0f;
                public readonly float HealFactor = 0.166f;
                public readonly int TickNum = 3;
            }
            public EchoOfLightConstant EchoOfLight = new EchoOfLightConstant();
        }
        public static readonly PriestConstant Priest = new PriestConstant();
        
        
        
    
    }
}
