﻿using HAJE.WowArena.Network;
using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HAJE.WowArena.Simulation
{
    class OutputBuilder
    {
        public void Begin(int tick)
        {
            message = Session.RunningSession.CreateGameTickMessage();
            message.Write(tick);
        }

        public void End()
        {
            message.Write((byte)SimulationCommand.End);
            Session.RunningSession.SendGameTickMessage(message);
        }

        #region 초기화 관련

        public void PlaceCharacter(int player, int character, int maxHp)
        {
            message.Write((byte)SimulationCommand.PlaceCharacter);
            message.Write((byte)player);
            message.Write((byte)character);
            message.Write((short)maxHp);
        }

        public void SetMana(int player, int value)
        {
            message.Write((byte)SimulationCommand.SetMana);
            message.Write((byte)player);
            message.Write((short)value);
        }

        #endregion

        public void RegenerateMana(int player, int regenValue, int manaAfter)
        {
            message.Write((byte)SimulationCommand.RegenerateMana);
            message.Write((byte)player);
            message.Write((short)regenValue);
            message.Write((short)manaAfter);
        }

        #region 전 캐릭터 범용

        public void Attack(int attackingPlayer, int attacker, int target, int skill,
            int damage, bool isMainTarget, bool isCritical, int hpAfter)
        {
            message.Write((byte)SimulationCommand.Attack);
            message.Write((byte)attackingPlayer);
            message.Write((byte)attacker);
            message.Write((byte)target);
            message.Write((byte)skill);
            message.Write((short)damage);
            message.Write((byte)(isMainTarget ? 1 : 0));
            message.Write((byte)(isCritical ? 1 : 0));
            message.Write((short)hpAfter);
        }

        public void InstantCast(int player, int caster, int cost, int manaAfter)
        {
            message.Write((byte)SimulationCommand.InstantCast);
            message.Write((byte)player);
            message.Write((byte)caster);
            message.Write((short)cost);
            message.Write((short)manaAfter);
        }

        public void BeginCast(int player, int caster, float time, int cost, int manaAfter)
        {
            message.Write((byte)SimulationCommand.BeginCast);
            message.Write((byte)player);
            message.Write((byte)caster);
            message.Write(time);
            message.Write((short)cost);
            message.Write((short)manaAfter);
        }

        public void CancelCast(int player, int caster)
        {
            message.Write((byte)SimulationCommand.CancelCast);
            message.Write((byte)player);
            message.Write((byte)caster);
        }

        public void EndCast(int player, int caster)
        {
            message.Write((byte)SimulationCommand.EndCast);
            message.Write((byte)player);
            message.Write((byte)caster);
        }

        public void AttachEffect(int player, int target, int effect, int value, float lifeTime)
        {
            message.Write((byte)SimulationCommand.AttachEffect);
            message.Write((byte)player);
            message.Write((byte)target);
            message.Write((byte)effect);
            message.Write((short)value);
            message.Write(lifeTime);
        }

        public void DetachEffect(int player, int target, int effect)
        {
            message.Write((byte)SimulationCommand.DetachEffect);
            message.Write((byte)player);
            message.Write((byte)target);
            message.Write((byte)effect);
        }

        public void UpgradeBegin(int player, int target, float time, int cost, int manaAfter)
        {
            message.Write((byte)SimulationCommand.UpgradeBegin);
            message.Write((byte)player);
            message.Write((byte)target);
            message.Write(time);
            message.Write((short)cost);
            message.Write((short)manaAfter);
        }

        public void UpgradeEnd(int player, int target, int levelAfter, int hpAfter, int hpMaxAfter)
        {
            message.Write((byte)SimulationCommand.UpgradeEnd);
            message.Write((byte)player);
            message.Write((byte)target);
            message.Write((byte)levelAfter);
            message.Write((short)hpAfter);
            message.Write((short)hpMaxAfter);
        }

        public void UpgradeCancelled(int player, int target, int refund, int manaAfter)
        {
            message.Write((byte)SimulationCommand.UpgradeCancelled);
            message.Write((byte)player);
            message.Write((byte)target);
            message.Write((short)refund);
            message.Write((short)manaAfter);
        }

        #endregion

        #region 특정 캐릭터 대상

        public void Heal(int player, int healer, int target, int skill,
            int healAmount, bool isMainTarget, bool isCritial, int hpAfter)
        {
            message.Write((byte)SimulationCommand.RegenerateMana);
            message.Write((byte)player);
            message.Write((byte)healer);
            message.Write((byte)target);
            message.Write((byte)skill);
            message.Write((short)healAmount);
            message.Write((byte)(isMainTarget ? 1 : 0));
            message.Write((byte)(isCritial ? 1 : 0));
            message.Write((short)hpAfter);
        }

        #endregion

        NetOutgoingMessage message;
    }
}
