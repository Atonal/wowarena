﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HAJE.WowArena.Simulation
{
    class Effect
    {
        public const int Aimed = 0;
        public const int Concentrated = 1;
        public const int Intervene = 2;
        public const int Vengence = 3;
        public const int DeepFreeze = 4;
        public const int DivineProtection = 5;
        public const int Renew = 6;
        public const int EchoOfLight = 7;
        public const int InterveneSelf = 8;
        

        public int id = -1;
        public float RemainTime = 0.0f;
        public float StackNumber = 0.0f;

        public Effect(int id)
        {
            this.id = id;
            EffectEnd += DummyFunction;
        }

        public delegate void EffectEndHandler(int player, int target);
        public EffectEndHandler EffectEnd;

        public void DummyFunction ( int player, int target)
        {
            return;
        }
    }
}
