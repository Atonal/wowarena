﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HAJE.WowArena.Simulation
{
    class Skill
    {
        public const int SteadyShot = 0;
        public const int AimedShot = 1;
        public const int Slam = 2;
        public const int Intervene = 3;
        public const int ArcaneTorrent = 4;
        public const int DeepFreeze = 5;
        public const int Consecration = 6;
        public const int DivineProtection = 7;
        public const int Heal = 8;
        public const int Renew = 9;
        public const int EchoOfLight = 10;

        public int id = -1;
    }
}
