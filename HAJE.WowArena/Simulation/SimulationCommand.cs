﻿
namespace HAJE.WowArena.Simulation
{
    enum SimulationCommand
    {
        End = 255,
        PlaceCharacter = 0,
        SetMana,
        RegenerateMana,
        Attack,
        InstantCast,
        BeginCast,
        CancelCast,
        EndCast,
        AttachEffect,
        DetachEffect,
        UpgradeBegin,
        UpgradeEnd,
        UpgradeCancelled,
        Heal
    }
}
