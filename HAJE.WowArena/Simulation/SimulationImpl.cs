﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HAJE.WowArena.Simulation
{
    class SimulationImpl : SimulationBase
    {
        public Player[] Players = new Player[2];
        public Character[][] Team = new Character[2][];
        Random Rand;

        
        protected override void InitializeSimulation()
        {
            Rand = new Random();
            for (int i = 0; i < Player.Max; i++)
            {
                Players[i] = new Player();
                Players[i].Mana = BalanceConstant.Global.initialMana;
                Team[i] = new Character[Character.Max];
                Output.SetMana(i, (int)Players[i].Mana);
               
                for (int j = 0; j < Character.Max; j++ )
                {
                    Team[i][j] = new Character();
                }
                Team[i][0].hp = BalanceConstant.Hunter.MaxHp[Team[i][0].level];
                Team[i][0].AttackCooldownConst = BalanceConstant.Hunter.Attack.Cooldown;
                Team[i][0].CritPercentage = (int)(BalanceConstant.Global.CriticalChance*100);
                Team[i][0].BasicAttack = SteadyShot;
                Team[i][0].MainTarget = Rand.Next(0, 5);
                Team[i][1].hp = BalanceConstant.Warrior.MaxHp[Team[i][1].level];
                Team[i][1].AttackCooldownConst = BalanceConstant.Warrior.Attack.Cooldown;
                Team[i][1].CritPercentage = (int)(BalanceConstant.Global.CriticalChance * 100);
                Team[i][1].BasicAttack = Slam;
                Team[i][1].MainTarget = Rand.Next(0, 5);
                Team[i][2].hp = BalanceConstant.Magician.MaxHp[Team[i][2].level];
                Team[i][2].AttackCooldownConst = BalanceConstant.Magician.Attack.Cooldown;
                Team[i][2].CritPercentage = (int)(BalanceConstant.Global.CriticalChance * 100);
                Team[i][2].BasicAttack = ArcaneTorrent;
                Team[i][2].MainTarget = Rand.Next(0, 5);
                Team[i][3].hp = BalanceConstant.Paladin.MaxHp[Team[i][3].level];
                Team[i][3].AttackCooldownConst = BalanceConstant.Paladin.Attack.Cooldown;
                Team[i][3].CritPercentage = (int)(BalanceConstant.Global.CriticalChance * 100);
                Team[i][3].BasicAttack = Consecration;
                Team[i][3].MainTarget = Rand.Next(0, 5);
                Team[i][4].hp = BalanceConstant.Priest.MaxHp[Team[i][4].level];
                Team[i][4].AttackCooldownConst = BalanceConstant.Priest.Attack.Cooldown;
                Team[i][4].CritPercentage = (int)(BalanceConstant.Global.CriticalChance * 100);
                Team[i][4].BasicAttack = Heal;
                Team[i][4].MainTarget = Rand.Next(0, 5);

                for(int j = 0; j < Character.Max; j++ )
                {
                    Output.PlaceCharacter(i, j, Team[i][j].hp);
                }
                
            }
        }

        protected override void SimulationTick(GameTime gameTime)
        {
            for ( int i = 0; i < Player.Max; i++)
            {
                int maxlevel = 0;

                for ( int j = 0; j < Character.Max; j++ )
                {
                    if (Team[i][j].LastAction == null && Team[i][j].EffectList.Find(x => x.id == Effect.DeepFreeze) == null && Team[i][j].LastUpgrade == null && Team[i][j].hp > 0)
                    {
                        Team[i][j].AttackCooldown -= gameTime.DeltaTime;
                    }
                    Team[i][j].SkillCooldown -= gameTime.DeltaTime;
                    for (int k = 0; k < Team[i][j].EffectList.Count; k++ )
                    {
                        Team[i][j].EffectList[k].RemainTime -= gameTime.DeltaTime;
                        if ( Team[i][j].EffectList[k].RemainTime < 0 )
                        {
                            Team[i][j].EffectList[k].EffectEnd(i, j);
                            Output.DetachEffect(i, j, Team[i][j].EffectList[k].id);
                            Team[i][j].EffectList.RemoveAt(k);
                        }
                    }
                    if (Team[i][j].AttackCooldown < 0)
                    {
                        if (j == Character.Priest)
                        {
                            while (Team[i][Team[i][j].MainTarget].hp <= 0)
                            {
                                Team[i][j].MainTarget = Rand.Next(0, Character.Max);
                            }
                        }
                        else
                        {
                            while (Team[OpponentTeam(i)][Team[i][j].MainTarget].hp <= 0)
                            {
                                Team[i][j].MainTarget = Rand.Next(0, Character.Max);
                            }
                        }
                        Team[i][j].BasicAttack(i, j, Team[i][j].MainTarget);
                        Team[i][j].AttackCooldown += Team[i][j].AttackCooldownConst;
                    }
                    if (Team[i][j].hp > 0)
                    {
                        if (Team[i][j].level > maxlevel)
                        {
                            maxlevel = Team[i][j].level;
                        }
                    }
                    Players[i].Mana += BalanceConstant.Global.ManaRegen[maxlevel]*gameTime.DeltaTime;
                    if (Players[i].Mana > 400)
                    {
                        Players[i].Mana = 400;
                    }
                    Output.RegenerateMana(i, (int)(BalanceConstant.Global.ManaRegen[maxlevel] * gameTime.DeltaTime), (int)Players[i].Mana );
                }
            }
        }

        #region ISimulationInput 구현

        public override void SetTarget(int player, int character, int targetedPlayer,int newTarget)
        {
            if (character == Character.Paladin)
            {
                if ( targetedPlayer != OpponentTeam(player))
                {
                    newTarget+= 5;
                }
                Team[player][character].MainTarget = newTarget;
            }
            else
            {
                if ( targetedPlayer != OpponentTeam(player) )
                {
                    //여기에서 에러 보내면 됨.
                }
                else
                {
                    Team[player][character].MainTarget = newTarget;
                }
            }
        }

        public override void UseSkill( int player, int caster, int target)
        {

            if ( Team[player][caster].hp <= 0)
            {
                return;
            }

            if (Team[player][caster].EffectList.Find(x => x.id == Effect.DeepFreeze) != null)
            {
                return;
            }

            switch( caster )
            {
                case Character.Hunter:
                    while (Team[OpponentTeam(player)][target].hp <= 0)
                    {
                        target = Rand.Next(0, Character.Max);
                    }

                    if ( Players[player].Mana >= BalanceConstant.Hunter.AimedShot.Cost && Team[player][caster].SkillCooldown <= 0 )
                    {
                        Players[player].Mana-= BalanceConstant.Hunter.AimedShot.Cost;
                        Team[player][caster].SkillCooldown = BalanceConstant.Hunter.AimedShot.Cooldown;
                        Output.BeginCast(player, caster, BalanceConstant.Hunter.AimedShot.Casting, (int)BalanceConstant.Hunter.AimedShot.Cost, (int)Players[player].Mana);
                        Team[player][caster].LastAction = Scheduler.ScheduleOnce(() => { AimShot(player, caster, target); Output.EndCast(player, Character.Hunter); }, (Second)BalanceConstant.Hunter.AimedShot.Casting);
                        Team[OpponentTeam(player)][Character.Mage].SkillCooldown -= 5.0f;
                     }
                    break;
                case Character.Warrior:
                    while (Team[player][target].hp <= 0)
                    {
                        target = Rand.Next(0, Character.Max);
                    }

                    if ( Players[player].Mana >= BalanceConstant.Warrior.Intervene.Cost && Team[player][caster].SkillCooldown <= 0 )
                    {
                        Players[player].Mana-= BalanceConstant.Warrior.Intervene.Cost;
                        Team[player][caster].SkillCooldown = BalanceConstant.Warrior.Intervene.Cooldown;
                        Output.InstantCast(player, caster, BalanceConstant.Warrior.Intervene.Cost, (int)Players[player].Mana);
                        Intervene(player, caster, target);
                        Team[OpponentTeam(player)][Character.Mage].SkillCooldown -= 5.0f;
                    }
                    break;
                case Character.Mage:
                    while (Team[OpponentTeam(player)][target].hp <= 0)
                    {
                        target = Rand.Next(0, Character.Max);
                    }

                    if (Players[player].Mana >= BalanceConstant.Magician.DeepFreeze.Cost && Team[player][caster].SkillCooldown <= 0)
                    {
                        Players[player].Mana -= BalanceConstant.Magician.DeepFreeze.Cost;
                        Team[player][caster].SkillCooldown = BalanceConstant.Magician.DeepFreeze.Cooldown;
                        Output.InstantCast(player, caster, BalanceConstant.Magician.DeepFreeze.Cost, (int)Players[player].Mana);
                        DeepFreeze(player, caster, target);
                        Team[OpponentTeam(player)][Character.Mage].SkillCooldown -= 5.0f;
                    }
                    break;
                case Character.Paladin:
                    while (Team[player][target].hp <= 0)
                    {
                        target = Rand.Next(0, Character.Max);
                    }

                    if (Players[player].Mana >= BalanceConstant.Paladin.DivineProtection.Cost && Team[player][caster].SkillCooldown <= 0)
                    {
                        Players[player].Mana -= BalanceConstant.Paladin.DivineProtection.Cost;
                        Team[player][caster].SkillCooldown = BalanceConstant.Paladin.DivineProtection.Cooldown;
                        Output.BeginCast(player, caster, BalanceConstant.Paladin.DivineProtection.Casting , BalanceConstant.Paladin.DivineProtection.Cost, (int)Players[player].Mana);
                        Team[player][caster].LastAction = Scheduler.ScheduleOnce(() => { DivineProtection(player, caster, target); Output.EndCast(player, Character.Paladin); }, (Second)BalanceConstant.Paladin.DivineProtection.Casting);
                        Team[OpponentTeam(player)][Character.Mage].SkillCooldown -= 5.0f;
                    }
                    break;
                case Character.Priest:
                    while (Team[player][target].hp <= 0)
                    {
                        target = Rand.Next(0, Character.Max);
                    }

                    if (Players[player].Mana >= BalanceConstant.Priest.Renew.Cost && Team[player][caster].SkillCooldown <= 0)
                    {
                        Players[player].Mana -= BalanceConstant.Priest.Renew.Cost;
                        Team[player][caster].SkillCooldown = BalanceConstant.Priest.Renew.Cooldown;
                        Output.BeginCast(player, caster, BalanceConstant.Priest.Renew.Casting, BalanceConstant.Priest.Renew.Cost, (int)Players[player].Mana);
                        Team[player][caster].LastAction = Scheduler.ScheduleOnce(() => { Renew(player, caster, target); Output.EndCast(player, Character.Priest); }, (Second)BalanceConstant.Priest.Renew.Casting);
                        Team[OpponentTeam(player)][Character.Mage].SkillCooldown -= 5.0f;
                    }
                    break;
            }
        }

        public int OpponentTeam( int player )
        {
            if ( player == 0 )
            {
                return 1;
            }
            if (player == 1)
            {
                return 0;
            }
            else
            {
                return -1;
            }
        }


        //사냥꾼 
        public void SteadyShot( int player, int caster, int target)
        {
            int damage = (int)(BalanceConstant.Hunter.Attack.Damage[Team[player][caster].level]);
            Effect newEffect = Team[player][caster].EffectList.Find(x => x.id == Effect.Concentrated);
            if ( newEffect!= null)
            {
                newEffect.StackNumber += 1;
                newEffect.RemainTime = BalanceConstant.Hunter.Concentration.RemainTime;
                if ( newEffect.StackNumber > BalanceConstant.Hunter.Concentration.MaxStack)
                {
                    newEffect.StackNumber = BalanceConstant.Hunter.Concentration.MaxStack;
                }
            }
            else
            {
                newEffect = new Effect(Effect.Concentrated);
                newEffect.RemainTime = BalanceConstant.Hunter.Concentration.RemainTime;
                Team[player][caster].EffectList.Add(newEffect);
            }
            Output.AttachEffect(player, caster, Effect.Concentrated, (int)newEffect.StackNumber, BalanceConstant.Hunter.Concentration.RemainTime);
            bool isMainTarget = false;
            if (Team[player][caster].MainTarget == target) isMainTarget = true;
            Damaging(player, caster, target, damage, Skill.SteadyShot, isMainTarget);
        }

        public void AimShot(int player, int caster, int target)
        {
               int damage = (int)(BalanceConstant.Hunter.AimedShot.Damage);
                Effect aimEffect = Team[OpponentTeam(player)][target].EffectList.Find(x => x.id == Effect.Aimed);
                Output.EndCast(player, caster);
                Team[player][caster].LastAction = null;
                if ( aimEffect != null )
                {
                    int stack = (int)aimEffect.StackNumber;
                    damage += (int)(BalanceConstant.Hunter.AimedShot.StackDamageBonus[Team[player][caster].level] * stack);
                    aimEffect.StackNumber += 1;
                    aimEffect.RemainTime = BalanceConstant.Hunter.AimedShot.RemainTime;
                    if ( aimEffect.StackNumber > BalanceConstant.Hunter.AimedShot.MaxStack)
                    {
                        aimEffect.StackNumber = BalanceConstant.Hunter.AimedShot.MaxStack;
                    }
                }
                else
                {
                    aimEffect = new Effect(Effect.Aimed);
                    aimEffect.StackNumber = 1;
                    aimEffect.RemainTime = BalanceConstant.Hunter.AimedShot.RemainTime;
                    if(Team[OpponentTeam(player)][target].EffectList.Find(x => x.id == Effect.DivineProtection) == null)
                        Team[OpponentTeam(player)][target].EffectList.Add(aimEffect);
                }
                if (Team[player][caster].EffectList.Find(x => x.id == Effect.Concentrated) != null)
                {
                    Effect Concentrated = Team[player][caster].EffectList.Find(x => x.id == Effect.Concentrated);
                    int stack = (int)Concentrated.StackNumber;
                    damage += (int)(BalanceConstant.Hunter.Concentration.StackDamageBonus[Team[player][caster].level] * stack);
                    Team[player][caster].EffectList.Remove(Concentrated);
                    Output.DetachEffect(player, target, Concentrated.id);
                }
                if (Team[OpponentTeam(player)][target].EffectList.Find(x => x.id == Effect.DivineProtection) == null)
                    Output.AttachEffect(player, target, aimEffect.id, (int)aimEffect.StackNumber, aimEffect.RemainTime) ;
                bool isMainTarget = false;
                if ( Team[player][caster].MainTarget == target ) isMainTarget = true;
                Damaging(player, caster, target, damage, Skill.AimedShot, isMainTarget);
            
        }

        //전사

        public void Slam ( int player, int caster, int target )
        {
            int damage = (int)(BalanceConstant.Warrior.Attack.Damage[Team[player][caster].level]);
            Effect newEffect = Team[player][caster].EffectList.Find(x => x.id == Effect.Vengence);
            if (newEffect != null)
            {
                damage += (int)newEffect.StackNumber;
                Team[player][caster].EffectList.Remove(newEffect);
                Output.DetachEffect(player, target, Effect.Vengence);
            }
            bool isMainTarget = false;
            if (Team[player][caster].MainTarget == target) isMainTarget = true;
            Damaging(player, caster, target, damage, Skill.Slam, isMainTarget);
        }

        public void InterveneSelfEnd(int player, int target)
        {
            Team[player][target].DamageTakenAmplify /= BalanceConstant.Warrior.Intervene.DamageReduce;
        }

        public void Intervene (int player, int caster, int target)
        {
            if ( target == Character.Warrior)
            {
                Effect Intervene = new Effect(Effect.InterveneSelf);
                Intervene.EffectEnd += InterveneSelfEnd;
                Team[player][target].DamageTakenAmplify *= BalanceConstant.Warrior.Intervene.DamageReduce;
                Intervene.RemainTime = BalanceConstant.Warrior.Intervene.RemainTime[Team[player][caster].level];
                Output.AttachEffect(player, target, Intervene.id, 1, Intervene.RemainTime);
            }
            else
            {
                Effect Intervene;
                if (Team[player][target].EffectList.Find(x => x.id == Effect.Intervene) != null )
                {
                    Intervene = Team[player][target].EffectList.Find(x => x.id == Effect.Intervene);
                }
                else
                {
                    Intervene = new Effect(Effect.Intervene);
                    Team[player][target].EffectList.Add(Intervene);
                }
                Intervene.RemainTime = BalanceConstant.Warrior.Intervene.RemainTime[Team[player][caster].level];
                Output.AttachEffect(player, target, Intervene.id, 1, Intervene.RemainTime);
            }
        }

        //마법사

        public void ArcaneTorrent(int player, int caster, int target)
        {
            int damage = (int)(BalanceConstant.Magician.Attack.Damage[Team[player][caster].level]);
            Damaging(player, caster, target, damage, Skill.ArcaneTorrent, true);
            for (int i = 0; i < 2; i++)
            {
                int newTarget = Rand.Next(0, Character.Max);
                while(Team[OpponentTeam(player)][newTarget].hp <= 0)
                    newTarget = Rand.Next(0, Character.Max);
                Damaging(player, caster, newTarget, damage, Skill.ArcaneTorrent, false);
            }
        }

        public void DeepFreeze(int player, int caster, int target)
        {
            if (Team[OpponentTeam(player)][target].EffectList.Find(x => x.id == Effect.DivineProtection) == null)
            {
                Effect DeepFreeze;
                if (Team[OpponentTeam(player)][target].EffectList.Find(x => x.id == Effect.DeepFreeze) != null)
                {
                    DeepFreeze = Team[OpponentTeam(player)][target].EffectList.Find(x => x.id == Effect.DeepFreeze);
                }
                else
                {
                    DeepFreeze = new Effect(Effect.DeepFreeze);
                    Team[OpponentTeam(player)][target].EffectList.Add(DeepFreeze);
                }
                DeepFreeze.RemainTime = BalanceConstant.Magician.DeepFreeze.RemainTime[Team[player][caster].level];
                Output.AttachEffect(OpponentTeam(player), target, DeepFreeze.id, 1, DeepFreeze.RemainTime);
            }
        }

        //성기사

        public void Consecration(int player, int caster, int target)
        {
            if (target < Character.Max)
            {
                int damage = (int)(BalanceConstant.Paladin.Attack.Damage[Team[player][caster].level]);
                Damaging(player, caster, target, damage, Skill.Consecration, true);
                for (int i = 0; i < Character.Max; i++)
                {
                    float multiplier = BalanceConstant.Paladin.Attack.SplashPercent[Rand.Next(0, 4)];
                    if (Team[OpponentTeam( player)][i].hp > 0 )
                    {
                        Damaging(player, caster, i, (int)(damage * multiplier), Skill.Consecration, false);
                    }
                }
            }
            else
            {
                //아군타겟
                target -= 5;
                int heal = (int)(BalanceConstant.Paladin.Attack.Damage[Team[player][caster].level]);
                Healing(player, caster, target, heal, Skill.Consecration,true);
                for (int i = 0; i < Character.Max; i++)
                {
                    float multiplier = BalanceConstant.Paladin.Attack.SplashPercent[Rand.Next(0, 4)];
                    if (Team[player][i].hp > 0)
                    {
                        Healing(player, caster, i, (int)(heal * multiplier), Skill.Consecration, false);
                    }
                }
            }
        }

        public void DivineProtectionEnd(int player, int target)
        {
            Team[player][target].DamageDoneAmplify /= BalanceConstant.Paladin.DivineProtection.DamageAmplify;
            Team[player][target].DamageTakenAmplify /= BalanceConstant.Paladin.DivineProtection.DamageReduction;
        }

        public void DivineProtection(int player, int caster, int target)
        {
            Effect DivineProtection = new Effect(Effect.DivineProtection);
            Output.EndCast(player, caster);
            Team[player][caster].LastAction = null;
            DivineProtection.EffectEnd += DivineProtectionEnd;
            Team[player][target].DamageDoneAmplify *= BalanceConstant.Paladin.DivineProtection.DamageAmplify;
            Team[player][target].DamageTakenAmplify *= BalanceConstant.Paladin.DivineProtection.DamageReduction;
            DivineProtection.RemainTime = BalanceConstant.Paladin.DivineProtection.RemainTime[Team[player][caster].level];
            //조준사격 해제
            Effect aimed = Team[player][target].EffectList.Find(x => x.id == Effect.Aimed);
            if(aimed != null)
            {
                Team[player][target].EffectList.Remove(aimed);
                Output.DetachEffect(player, target, aimed.id);
            }
            //동결 해제
            Effect frozen = Team[player][target].EffectList.Find(x => x.id == Effect.DeepFreeze);
            if(frozen != null)
            {
                Team[player][target].EffectList.Remove(frozen);
                Output.DetachEffect(player, target, frozen.id);
            }
            Output.AttachEffect(player, target, DivineProtection.id, 1, DivineProtection.RemainTime);

        }

        //사제

        public void Heal(int player, int caster, int target)
        {
            int heal = (int)(BalanceConstant.Priest.Attack.Heal[Team[player][caster].level]);
            Healing(player, caster, target, heal, Skill.Heal, true);
        }

        public void Renew(int player, int caster, int target)
        {
            int heal = (int)(BalanceConstant.Priest.Attack.Heal[Team[player][caster].level]);
            for ( int i = 1; i < BalanceConstant.Priest.Renew.TickNum+1; i++ )
            {
                Scheduler.ScheduleOnce(() => { Healing(player, caster, target, (int)BalanceConstant.Priest.Renew.TickHeal[Team[player][caster].level], Skill.Renew,true ); }, (Second)i * BalanceConstant.Priest.Renew.TickTime  );
            }
            Effect RenewEffect = Team[player][target].EffectList.Find(x => x.id == Effect.Renew);
            if ( RenewEffect == null )
            {
                RenewEffect = new Effect(Effect.Renew);
                Team[player][target].EffectList.Add(RenewEffect);
            }
            RenewEffect.RemainTime = BalanceConstant.Priest.Renew.RemainTime;
            Output.AttachEffect(player, target, Effect.Renew, 1, BalanceConstant.Priest.Renew.RemainTime);
        }

            /*public const int SteadyShot = 0;
            public const int AimedShot = 1;
            public const int Smash = 2;
            public const int Intervene = 3;
            public const int ArcaneTorrent = 4;
            public const int DeepFreeze = 5;
            public const int Consecration = 6;
            public const int DivineProtection = 7;
            public const int Heal = 8;
            public const int Renew = 9;*/

        public void LevelUp(int player, int levelUpCharacter)
        {
            Character selectedCharacter = Team[player][levelUpCharacter];
            selectedCharacter.level++;
            int nowLevel = selectedCharacter.level;
            int prevLevel = nowLevel - 1;
            int maxHp = 0;
            switch( levelUpCharacter )
            {
                case Character.Hunter:
                    Team[player][levelUpCharacter].hp += BalanceConstant.Hunter.MaxHp[nowLevel] - BalanceConstant.Hunter.MaxHp[prevLevel];
                    maxHp = BalanceConstant.Hunter.MaxHp[nowLevel];
                    break;
                case Character.Warrior:
                    Team[player][levelUpCharacter].hp += BalanceConstant.Warrior.MaxHp[nowLevel] - BalanceConstant.Warrior.MaxHp[prevLevel];
                    maxHp = BalanceConstant.Warrior.MaxHp[nowLevel];
                    break;
                case Character.Mage:
                    Team[player][levelUpCharacter].hp += BalanceConstant.Magician.MaxHp[nowLevel] - BalanceConstant.Magician.MaxHp[prevLevel];
                    maxHp = BalanceConstant.Magician.MaxHp[nowLevel];
                    break;
                case Character.Paladin:
                    Team[player][levelUpCharacter].hp += BalanceConstant.Paladin.MaxHp[nowLevel] - BalanceConstant.Paladin.MaxHp[prevLevel];
                    maxHp = BalanceConstant.Paladin.MaxHp[nowLevel];
                    break;
                case Character.Priest:
                    Team[player][levelUpCharacter].hp += BalanceConstant.Priest.MaxHp[nowLevel] - BalanceConstant.Priest.MaxHp[prevLevel];
                    maxHp = BalanceConstant.Priest.MaxHp[nowLevel];
                    break;
            }
            Team[player][levelUpCharacter].AttackCooldown = Team[player][levelUpCharacter].AttackCooldownConst;
            Output.UpgradeEnd(player, levelUpCharacter, nowLevel, Team[player][levelUpCharacter].hp, maxHp);
        }

        public override void Upgrade(int player, int levelUpCharacter)
        {
            int realLevel = Team[player][levelUpCharacter].level;
            if (realLevel <= Character.levelMax && Players[player].Mana > (int)BalanceConstant.Global.UpgradeCost[realLevel] )
            {
                Players[player].Mana -= (int)BalanceConstant.Global.UpgradeCost[realLevel];
                Output.UpgradeBegin(player, levelUpCharacter, BalanceConstant.Global.UpgradeTime[realLevel], (int)BalanceConstant.Global.UpgradeCost[realLevel], (int)Players[player].Mana);
                Team[player][levelUpCharacter].LastUpgrade = Scheduler.ScheduleOnce(() => { LevelUp(player,levelUpCharacter); }, (Second)BalanceConstant.Global.UpgradeTime[realLevel]);
            }
        }

        public void Damaging( int player, int caster, int target, int damage, int skill, bool isMainTarget )
        {
            if (isMainTarget)
            {
                if(Team[OpponentTeam(player)][target].hp <= 0)
                {
                    int temp = target;
                    while (temp == target)
                        temp = Rand.Next(0, Character.Max);
                    target = temp;
                }
            }

            float RealDamage = (float)damage;
            bool isCrit = false;
            
            RealDamage = RealDamage * (1 - BalanceConstant.Global.DamageVariation) + RealDamage * 2 * ( Rand.NextFloat(BalanceConstant.Global.DamageVariation));

            if ( Rand.Next(0,100) < Team[player][caster].CritPercentage )
            {
                RealDamage = RealDamage * BalanceConstant.Global.CriticalDamage;
                isCrit = true;
            }

            if ( target == Character.Warrior)
            {
                Effect newVengence = Team[OpponentTeam(player)][target].EffectList.Find(x => x.id == Effect.Vengence);
                if ( newVengence == null )
                {
                    newVengence = new Effect(Effect.Vengence);
                    newVengence.StackNumber = RealDamage * BalanceConstant.Warrior.Vengence.StackDamageBonus;
                    newVengence.RemainTime = BalanceConstant.Warrior.Vengence.RemainTime;
                    Team[OpponentTeam(player)][target].EffectList.Add(newVengence);
                    Output.AttachEffect(OpponentTeam(player), target, newVengence.id, (int)newVengence.StackNumber, BalanceConstant.Warrior.Vengence.RemainTime);
                }
                else
                {
                    newVengence.StackNumber+= RealDamage * BalanceConstant.Warrior.Vengence.StackDamageBonus;
                    newVengence.RemainTime = BalanceConstant.Warrior.Vengence.RemainTime;
                }
            }

            if ( Team[OpponentTeam(player)][target].EffectList.Find(x=>x.id == Effect.Intervene ) != null )
            {
                Damaging(player, caster, Character.Warrior, damage, skill, isMainTarget);
                if ( Team[OpponentTeam(player)][Character.Warrior].hp <= 0 )
                {
                    Effect intervene = Team[OpponentTeam(player)][target].EffectList.Find(x => x.id == Effect.Intervene);
                    Team[OpponentTeam(player)][target].EffectList.Remove(intervene);
                    Output.DetachEffect(OpponentTeam(player), target, Effect.Intervene);
                }
                return;
            }
            
            RealDamage = RealDamage * Team[player][caster].DamageDoneAmplify * Team[OpponentTeam(player)][target].DamageTakenAmplify;
            Team[OpponentTeam(player)][target].hp -= (int)RealDamage;

            if (Team[OpponentTeam(player)][target].hp < 0)
            {
                Output.Attack(player, caster, target, skill, (int)RealDamage, isMainTarget, isCrit, 0);
                for( int i = 0; i < Team[OpponentTeam(player)][target].EffectList.Count; i++ )
                {
                    Output.DetachEffect(player, target, Team[OpponentTeam(player)][target].EffectList[i].id);
                }
                Team[OpponentTeam(player)][target].EffectList.Clear();
            }
            else
            {
                Output.Attack(player, caster, target, skill, (int)RealDamage, isMainTarget, isCrit, Team[OpponentTeam(player)][target].hp);
            }
        }

        public float Healing( int player, int caster, int target, int heal, int skill, bool isMainTarget )
        {

            if (isMainTarget)
            {
                while (Team[player][target].hp <= 0)
                {
                    target = Rand.Next(0, Character.Max);
                }
            }

            float RealHeal = (float)heal;
            bool isCrit = false;

            RealHeal = RealHeal * (1 - BalanceConstant.Global.DamageVariation) + RealHeal * 2 * (Rand.NextFloat(BalanceConstant.Global.DamageVariation));

            if (Rand.Next(0, 100) < Team[player][caster].CritPercentage)
            {
                RealHeal = RealHeal * BalanceConstant.Global.CriticalDamage;
                isCrit = true;
            }

            RealHeal = RealHeal * Team[player][caster].DamageDoneAmplify;

            GameSystem.Log.WriteLine("*******************");
            GameSystem.Log.WriteLine(RealHeal.ToString("R"));
            GameSystem.Log.WriteLine("===================");

            GameSystem.Log.WriteLine(Team[player][target].hp.ToString());

            Team[player][target].hp += (int)RealHeal;

            GameSystem.Log.WriteLine(Team[player][target].hp.ToString());

            int nowLevel = Team[player][target].level;
            int maxHp;
            switch (target)
            {
                case Character.Hunter:
                    maxHp = BalanceConstant.Hunter.MaxHp[nowLevel];
                    break;
                case Character.Warrior:
                    maxHp = BalanceConstant.Warrior.MaxHp[nowLevel];
                    break;
                case Character.Mage:
                    maxHp = BalanceConstant.Magician.MaxHp[nowLevel];
                    break;
                case Character.Paladin:
                    maxHp = BalanceConstant.Paladin.MaxHp[nowLevel];
                    break;
                case Character.Priest:
                    maxHp = BalanceConstant.Priest.MaxHp[nowLevel];
                    break;
                default:
                    maxHp = 0;
                    break;
            }

            if( Team[player][target].hp > maxHp)
            {
                RealHeal -= Team[player][target].hp - maxHp;
                Team[player][target].hp = maxHp;
            }

            if (Team[player][target].hp > 0)
            {
                Output.Heal(player, caster, target, skill, (int)RealHeal, isMainTarget, isCrit, Team[player][target].hp);
            }

            
            // 사제 스킬일 경우
            if ( skill == Skill.Renew || skill == Skill.Heal )
            {
                for (int i = 1; i < BalanceConstant.Priest.EchoOfLight.TickNum + 1; i++)
                {
                    Scheduler.ScheduleOnce(() => { Healing(player, caster, target, (int)(RealHeal * BalanceConstant.Priest.EchoOfLight.HealFactor), Skill.EchoOfLight, isMainTarget); }, (Second)i * BalanceConstant.Priest.EchoOfLight.TickTime);
                }
                Effect EchoOfLightEffect = Team[player][target].EffectList.Find(x => x.id == Effect.EchoOfLight);
                if (EchoOfLightEffect == null)
                {
                    EchoOfLightEffect = new Effect(Effect.EchoOfLight);
                    Team[player][target].EffectList.Add(EchoOfLightEffect);
                }
                EchoOfLightEffect.RemainTime = BalanceConstant.Priest.EchoOfLight.RemainTime;
                Output.AttachEffect(player, target, Effect.EchoOfLight, 1, BalanceConstant.Priest.EchoOfLight.RemainTime);
            }
            

            return RealHeal;
        }

        #endregion
    }
}
