﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HAJE.WowArena.Simulation
{
    abstract class SimulationBase : IUpdatable
    {
        public static readonly Second LogicTick = (Second)0.20f;

        public void Start()
        {
            SimulationTickCount = 0;

            Output.Begin(SimulationTickCount);
            InitializeSimulation();
            Output.End();
            SimulationTickCount++;

            nextFrame = LogicTick;
        }

        public void Update(GameTime gameTime)
        {
            // 고정프레임으로 SimulationTick 호출
            nextFrame -= gameTime.DeltaTime;
            while (nextFrame < 0)
            {
                logicTime.SetDeltaTime(LogicTick);

                Output.Begin(SimulationTickCount);

                foreach (var a in SimulationInputQueue)
                {
                    a();
                }
                SimulationInputQueue.Clear();

                Scheduler.Update(logicTime);
                SimulationTick(logicTime);
                Output.End();

                SimulationTickCount++;
                nextFrame += LogicTick;
            }
        }

        protected readonly Scheduler Scheduler = new Scheduler();
        public readonly OutputBuilder Output = new OutputBuilder();

        protected abstract void InitializeSimulation();
        protected abstract void SimulationTick(GameTime gameTime);

        public int SimulationTickCount { get; set; }
        public List<Action> SimulationInputQueue = new List<Action>();

        public abstract void SetTarget(int player, int character, int targetedPlayer, int newTarget);
        public abstract void UseSkill(int player, int caster, int target);
        public abstract void Upgrade(int player, int levelUpCharacter);

        Second nextFrame = (Second)0;
        GameTime logicTime = new GameTime();
    }
}
