﻿using SharpDX;
using SharpDX.Windows;
using System;
using System.Drawing;
using System.Windows.Forms;
using Point = System.Drawing.Point;

namespace HAJE.WowArena.Input
{
    #region public enum MouseButtons

    public enum MouseButtons
    {
        None,
        Left,
        Right
    }

    #endregion

    public delegate void MouseButtonHandler(MouseButtons button, int x, int y);

    public class Mouse
    {
        public Mouse(RenderForm form)
        {
            isPressed = new bool[Enum.GetValues(typeof(MouseButtons)).Length];
            for (int i = 0; i < isPressed.Length; i++)
                isPressed[i] = false;
            visible = true;

            form.Deactivate += new EventHandler(form_Deactivate);
            form.Click += new EventHandler(form_Click);
            form.DoubleClick += new EventHandler(form_DoubleClick);
            form.MouseMove += new MouseEventHandler(form_MouseMove);
            form.MouseDown += new MouseEventHandler(form_MouseDown);
            form.MouseUp += new MouseEventHandler(form_MouseUp);
        }

        public int X
        {
            get
            {
                return x;
            }
            set
            {
                x = value;
                Cursor.Position = new Point(x, y);
            }
        }

        public int Y
        {
            get
            {
                return y;
            }
            set
            {
                y = value;
                Cursor.Position = new Point(x, y);
            }
        }

        public Vector2 Position
        {
            get
            {
                return new Vector2(x, y);
            }
            set
            {
                x = (int)value.X;
                y = (int)value.Y;
                Cursor.Position = new Point(x, y);
            }
        }

        public bool IsButtonPressed(MouseButtons button)
        {
            return isPressed[(int)button];
        }

        public bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                if (visible != value)
                {
                    visible = value;
                    if (visible)
                        Cursor.Show();
                    else
                        Cursor.Hide();
                }
            }
        }

        public event MouseButtonHandler Click = delegate { };

        public event MouseButtonHandler DoubleClick = delegate { };

        public event MouseButtonHandler ButtonDown = delegate { };

        public event MouseButtonHandler ButtonUp = delegate { };

        #region privates

        void form_MouseUp(object sender, MouseEventArgs e)
        {
            SetMousePosition(e);
            MouseButtons b = ConvertMouseButtons(e.Button);
            if (b != MouseButtons.None && isPressed[(int)b])
            {
                isPressed[(int)b] = false;
                ButtonUp(b, x, y);
            }
        }

        void form_MouseDown(object sender, MouseEventArgs e)
        {
            SetMousePosition(e);
            MouseButtons b = ConvertMouseButtons(e.Button);
            if (b != MouseButtons.None && !isPressed[(int)b])
            {
                isPressed[(int)b] = true;
                ButtonDown(b, x, y);
            }
        }

        void form_MouseMove(object sender, MouseEventArgs e)
        {
            SetMousePosition(e);
        }

        void form_DoubleClick(object sender, EventArgs e)
        {
            MouseEventArgs me = e as MouseEventArgs;
            SetMousePosition(me);
            MouseButtons b = ConvertMouseButtons(me.Button);
            if (b != MouseButtons.None)
                DoubleClick(b, x, y);
        }

        void form_Click(object sender, EventArgs e)
        {
            MouseEventArgs me = e as MouseEventArgs;
            SetMousePosition(me);
            MouseButtons b = ConvertMouseButtons(me.Button);
            if (b != MouseButtons.None)
                Click(b, x, y);
        }

        /// <summary>
        /// 마우스 버튼이 눌린 뒤, 창의 포커스가 옮겨지고 버튼이 떼어지는 경우,
        /// 영원히 버튼이 눌린 상태가 지속되는 문제를 해결하기 위해
        /// 창이 포커스를 잃으면 마우스 버튼을 떼진 것으로 간주하기 위한 메서드.
        /// </summary>
        void form_Deactivate(object sender, EventArgs e)
        {
            if (isPressed[(int)MouseButtons.Left])
            {
                isPressed[(int)MouseButtons.Left] = false;
                ButtonUp(MouseButtons.Left, x, y);
            }
            if (isPressed[(int)MouseButtons.Right])
            {
                isPressed[(int)MouseButtons.Right] = false;
                ButtonUp(MouseButtons.Right, x, y);
            }
        }

        void SetMousePosition(MouseEventArgs e)
        {
            x = e.X;
            y = e.Y;
        }

        static MouseButtons ConvertMouseButtons(System.Windows.Forms.MouseButtons buttons)
        {
            if (buttons == System.Windows.Forms.MouseButtons.Left)
                return MouseButtons.Left;
            else if (buttons == System.Windows.Forms.MouseButtons.Right)
                return MouseButtons.Right;
            return MouseButtons.None;
        }

        int x, y;
        bool[] isPressed;
        bool visible;

        #endregion
    }
}
