﻿using System.IO;
using System.Xml.Serialization;

namespace HAJE.WowArena
{
    public class Configuration
    {
        [XmlAttribute]
        public string appName = "arena";

        [XmlAttribute]
        public int port = 12312;

        public enum ConnectionType
        {
            auto,
            forceHost,
            forceGuest,
            localTest
        }

        [XmlAttribute]
        public ConnectionType connectionType = ConnectionType.auto;

        public struct HostAddress
        {
            [XmlAttribute]
            public string ip;

            [XmlAttribute]
            public int port;
        }

        public HostAddress[] ConnectionCandidates = new HostAddress[0];

        public static Configuration Load()
        {
            string path = "config.xml";

            XmlSerializer serializer = new XmlSerializer(typeof(Configuration));
            using (StreamReader reader = new StreamReader(path))
            {
                var ret = serializer.Deserialize(reader) as Configuration;
                var addrList = ret.ConnectionCandidates;
                for (int i = 0; i < addrList.Length; i++)
                {
                    var addr = addrList[i];
                    if (addr.port == 0)
                    {
                        addrList[i] = new HostAddress()
                        {
                            ip = addr.ip,
                            port = ret.port
                        };
                    }
                }

                return ret;
            }
        }
    }
}
