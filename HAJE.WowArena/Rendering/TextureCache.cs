﻿using SharpDX.Direct3D9;
using System;
using System.Collections.Generic;

namespace HAJE.WowArena.Rendering
{
    public class TextureCache : IDisposable
    {
        public TextureCache(Device graphicsDevice)
        {
            this.graphicsDevice = graphicsDevice;
        }

        public Texture GetTexture(string path)
        {
            Texture tex;
            if (cache.TryGetValue(path, out tex))
                return tex;
            tex = Texture.FromFile(graphicsDevice, path);
            cache.Add(path, tex);
            return tex;
        }

        public void Clear()
        {
            foreach (Texture t in cache.Values)
            {
                t.Dispose();
            }
            cache.Clear();
        }

        public void Dispose()
        {
            Clear();
        }

        Device graphicsDevice;
        Dictionary<string, Texture> cache = new Dictionary<string, Texture>();
    }
}
