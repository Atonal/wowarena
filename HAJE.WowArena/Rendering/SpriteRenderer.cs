﻿using HAJE.WowArena.Rendering.VertexTypes;
using SharpDX;
using SharpDX.Direct3D9;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HAJE.WowArena.Rendering
{
    class SpriteRenderer
    {
        public SpriteRenderer(Device graphicsDevice)
        {
            device = graphicsDevice;
            vertexBuffer = new VertexBuffer(device,
                triangleCount * 3 * TransformedPositionColorTextureVertexType.Size,
                Usage.WriteOnly,
                VertexFormat.None,
                Pool.Managed
            ); 
        }

        public void SetUp()
        {
            device.SetRenderState(RenderState.Lighting, false);
            device.SetRenderState(RenderState.ZEnable, false);
            device.SetSamplerState(0, SamplerState.MagFilter, TextureFilter.Linear);
            device.SetSamplerState(0, SamplerState.MinFilter, TextureFilter.Linear);
            device.SetSamplerState(0, SamplerState.MipFilter, TextureFilter.Linear);
            device.SetSamplerState(0, SamplerState.AddressU, TextureAddress.Clamp);
            device.SetSamplerState(0, SamplerState.AddressV, TextureAddress.Clamp);
            device.SetRenderState(RenderState.AlphaBlendEnable, true);
            device.SetRenderState(RenderState.SourceBlend, Blend.SourceAlpha);
            device.SetRenderState(RenderState.DestinationBlend, Blend.InverseSourceAlpha);
            device.SetRenderState(RenderState.SeparateAlphaBlendEnable, true);
            device.SetRenderState(RenderState.SourceBlendAlpha, Blend.SourceAlpha);
            device.SetRenderState(RenderState.DestinationBlendAlpha, Blend.InverseSourceAlpha);
            device.SetRenderState(RenderState.BlendOperationAlpha, BlendOperation.Add);
            device.SetStreamSource(0, vertexBuffer, 0, TransformedPositionColorTextureVertexType.Size);
            device.VertexDeclaration = TransformedPositionColorTextureVertexType.VertexDeclaration;
        }

        public void Draw(Sprite sprite)
        {
            using (DataStream buffer = vertexBuffer.Lock(0, 0, LockFlags.None))
            {
                sprite.WriteBuffer(buffer);
                vertexBuffer.Unlock();
            }
            

            device.SetTexture(0, sprite.Texture);
            device.DrawPrimitives(PrimitiveType.TriangleList, 0, triangleCount);
        }

        public void Dispose()
        {
            if (vertexBuffer != null) vertexBuffer.Dispose(); vertexBuffer = null;
        }

        Device device;
        const int triangleCount = 2;
        VertexBuffer vertexBuffer;
    }
}
