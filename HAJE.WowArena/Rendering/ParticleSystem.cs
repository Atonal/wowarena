﻿using HAJE.WowArena.Rendering.VertexTypes;
using SharpDX;
using SharpDX.Direct3D9;
using System;
using System.Collections.Generic;
using System.Drawing;
using Color = SharpDX.Color;

namespace HAJE.WowArena.Rendering
{
    struct Particle
    {
        public Vector2 Position;
        public Vector2 Velocity;
        public Vector2 Acceleration;
        public float Size;
        public Second LifeRemain;
        public Color Color;
    }

    class ParticleSystem : IUpdatable, IDisposable
    {
        class Particle
        {
            public Vector2 Position;
            public Vector2 Velocity;
            public Vector2 Acceleration = Vector2.Zero;
            public float Size = 6;
            public Second LifeRemain = (Second)4.0f;
            public Color Color = Color.White;
        }

        public ParticleSystem(Device device, string particlePath)
        {
            int width = 0, height = 0;
            using (Image image = Image.FromFile(particlePath))
            {
                width = image.Width;
                height = image.Height;
            }
            textureSize = new Vector2(width, height);
            texture = Texture.FromFile(device, particlePath);

            this.device = device;
            this.vertexBuffer = new VertexBuffer(device,
                maxParticle * trianglePerParticle * 3 * TransformedPositionColorTextureVertexType.Size,
                Usage.WriteOnly,
                VertexFormat.None,
                Pool.Managed
            ); 
        }

        public void AddParticle(Rendering.Particle p)
        {
            var newParticle = activeParticles.NewItem();
            newParticle.Velocity = p.Velocity;
            newParticle.Color = p.Color;
            newParticle.Acceleration = p.Acceleration;
            newParticle.LifeRemain = p.LifeRemain;
            newParticle.Position = p.Position;
            newParticle.Size = p.Size;
        }

        public void Update(GameTime gameTime)
        {
            foreach (var p in activeParticles)
            {
                p.Position += p.Velocity * gameTime.DeltaTime;
                p.Velocity += p.Acceleration * gameTime.DeltaTime;
                p.LifeRemain -= gameTime.DeltaTime;
                if (p.LifeRemain <= 0)
                {
                    deadParticle.Add(p);
                }
            }

            foreach (var p in deadParticle)
            {
                activeParticles.Recycle(p);
            }
            deadParticle.Clear();
        }

        public void Draw()
        {
            device.SetRenderState(RenderState.Lighting, false);
            device.SetRenderState(RenderState.ZEnable, false);
            device.SetSamplerState(0, SamplerState.MagFilter, TextureFilter.Linear);
            device.SetSamplerState(0, SamplerState.MinFilter, TextureFilter.Linear);
            device.SetSamplerState(0, SamplerState.MipFilter, TextureFilter.Linear);
            device.SetSamplerState(0, SamplerState.AddressU, TextureAddress.Clamp);
            device.SetSamplerState(0, SamplerState.AddressV, TextureAddress.Clamp);
            device.SetRenderState(RenderState.AlphaBlendEnable, true);
            device.SetRenderState(RenderState.SourceBlend, Blend.SourceAlpha);
            device.SetRenderState(RenderState.DestinationBlend, Blend.InverseSourceAlpha);
            device.SetRenderState(RenderState.SeparateAlphaBlendEnable, true);
            device.SetRenderState(RenderState.SourceBlendAlpha, Blend.SourceAlpha);
            device.SetRenderState(RenderState.DestinationBlendAlpha, Blend.InverseSourceAlpha);
            device.SetRenderState(RenderState.BlendOperationAlpha, BlendOperation.Add);
            device.SetStreamSource(0, vertexBuffer, 0, TransformedPositionColorTextureVertexType.Size);
            device.VertexDeclaration = TransformedPositionColorTextureVertexType.VertexDeclaration;
            device.SetTexture(0, texture);

            int particleCount = activeParticles.Count;
            int count = 0;
            DataStream buffer = vertexBuffer.Lock(0, 0, LockFlags.None);
            foreach (var p in activeParticles)
            {
                WriteParticle(p, buffer);
                count++;
                if (count == maxParticle)
                {
                    vertexBuffer.Unlock();
                    buffer.Dispose();
                    device.DrawPrimitives(PrimitiveType.TriangleList, 0, count * trianglePerParticle);
                    buffer = vertexBuffer.Lock(0, 0, LockFlags.None);
                    count = 0;
                }
            }
            vertexBuffer.Unlock();
            buffer.Dispose();
            device.DrawPrimitives(PrimitiveType.TriangleList, 0, count * trianglePerParticle);
            buffer = vertexBuffer.Lock(0, 0, LockFlags.None);
        }

        void WriteParticle(Particle p, DataStream buffer)
        {
            TransformedPositionColorTextureVertexType topLeft;
            TransformedPositionColorTextureVertexType topRight;
            TransformedPositionColorTextureVertexType bottomLeft;
            TransformedPositionColorTextureVertexType bottomRight;

            topLeft.Color = topRight.Color = bottomLeft.Color = bottomRight.Color = p.Color;
            topLeft.Position = topRight.Position = bottomLeft.Position = bottomRight.Position = new Vector4(p.Position, 0, 1);

            float radius = p.Size / 2;

            topLeft.TextureUV = new Vector2(0, 0);
            topLeft.Position.X -= radius;
            topLeft.Position.Y -= radius;

            topRight.TextureUV = new Vector2(1, 0);
            topRight.Position.X += radius;
            topRight.Position.Y -= radius;

            bottomLeft.TextureUV = new Vector2(0, 1);
            bottomLeft.Position.X -= radius;
            bottomLeft.Position.Y += radius;

            bottomRight.TextureUV = new Vector2(1, 1);
            bottomRight.Position.X += radius;
            bottomRight.Position.Y += radius;

            topLeft.WriteTo(buffer);
            topRight.WriteTo(buffer);
            bottomLeft.WriteTo(buffer);
            topRight.WriteTo(buffer);
            bottomRight.WriteTo(buffer);
            bottomLeft.WriteTo(buffer);
        }

        public void Dispose()
        {
            texture.Dispose();
            texture = null;
            activeParticles.Clear();
            vertexBuffer.Dispose();
            vertexBuffer = null;
        }

        Texture texture;
        Vector2 textureSize;
        Device device;
        Utility.RecycleList<Particle> activeParticles = new Utility.RecycleList<Particle>();
        List<Particle> deadParticle = new List<Particle>();

        const int trianglePerParticle = 2;
        const int maxParticle = 1000;
        VertexBuffer vertexBuffer;
    }
}
