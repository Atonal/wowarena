﻿using HAJE.WowArena.Rendering.VertexTypes;
using SharpDX;
using SharpDX.Direct3D9;

namespace HAJE.WowArena.Rendering
{
    class Sprite
    {
        public readonly Texture Texture;

        public bool Visible = true;
        

        Vector2 position = Vector2.Zero;
        public Vector2 Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
                vertexDirty = true;
            }
        }

        Vector2 anchor = Vector2.Zero;
        public Vector2 Anchor
        {
            get
            {
                return anchor;
            }
            set
            {
                anchor = value;
                vertexDirty = true;
            }
        }

        Vector2 scale = Vector2.One;
        public Vector2 Scale
        {
            get
            {
                return scale;
            }
            set
            {
                scale = value;
                vertexDirty = true;
            }
        }

        Color color = Color.White;
        public Color Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
                vertexDirty = true;
            }
        }

        Radian rotation = (Radian)0;
        public Radian Rotation
        {
            get
            {
                return rotation;
            }
            set
            {
                rotation = value;
                vertexDirty = true;
            }
        }

        public Sprite(Texture texture, System.Drawing.Size imageSize)
        {
            this.Texture = texture;
            this.imageSize = imageSize.ToVector2();
            this.textureSize = texture.GetSize();
        }

        public void WriteBuffer(DataStream buffer)
        {
            if (vertexDirty)
            {
                UpdateVertices();
                vertexDirty = false;
            }
            topLeft.WriteTo(buffer);
            topRight.WriteTo(buffer);
            bottomLeft.WriteTo(buffer);
            topRight.WriteTo(buffer);
            bottomRight.WriteTo(buffer);
            bottomLeft.WriteTo(buffer);
        }

        public void Draw(SpriteRenderer render)
        {
            if (Visible)
                render.Draw(this);
        }

        void UpdateVertices()
        {
            topLeft.Color = topRight.Color = bottomLeft.Color = bottomRight.Color = color;
            topLeft.Position = topRight.Position = bottomLeft.Position = bottomRight.Position = new Vector4(position, 0, 1);

            float scaledWidth = imageSize.X * scale.X;
            float scaledHeight = imageSize.Y * scale.Y;
            Vector2 tl = new Vector2(-anchor.X * scaledWidth, -anchor.Y * scaledHeight);
            Vector2 tr = new Vector2((1 - anchor.X) * scaledWidth, -anchor.Y * scaledHeight);
            Vector2 bl = new Vector2(-anchor.X * scaledWidth, (1 - anchor.Y) * scaledHeight);
            Vector2 br = new Vector2((1 - anchor.X) * scaledWidth, (1 - anchor.Y) * scaledHeight);
            tl = rotation.Rotate(tl);
            tr = rotation.Rotate(tr);
            bl = rotation.Rotate(bl);
            br = rotation.Rotate(br);

            topLeft.TextureUV = new Vector2(0, 0);
            topLeft.Position.X += tl.X;
            topLeft.Position.Y += tl.Y;

            topRight.TextureUV = new Vector2(1, 0);
            topRight.Position.X += tr.X;
            topRight.Position.Y += tr.Y;

            bottomLeft.TextureUV = new Vector2(0, 1);
            bottomLeft.Position.X += bl.X;
            bottomLeft.Position.Y += bl.Y;

            bottomRight.TextureUV = new Vector2(1, 1);
            bottomRight.Position.X += br.X;
            bottomRight.Position.Y += br.Y;
        }

        public Vector2 TopLeft
        {
            get
            {
                var tl = topLeft.Position;
                return new Vector2(tl.X, tl.Y);
            }
        }

        public Vector2 BottomRight
        {
            get
            {
                var br = bottomRight.Position;
                return new Vector2(br.X, br.Y);
            }
        }

        Vector2 imageSize;
        Vector2 textureSize;
        bool vertexDirty = true;
        TransformedPositionColorTextureVertexType topLeft;
        TransformedPositionColorTextureVertexType topRight;
        TransformedPositionColorTextureVertexType bottomLeft;
        TransformedPositionColorTextureVertexType bottomRight;
    }
}
