﻿using SharpDX.Direct3D9;

namespace HAJE.WowArena.Rendering.VertexTypes
{
    public class VertexTypeProvider
    {
        public static void Initialize(Device device)
        {
            TransformedPositionColorTextureVertexType.SetupDeclaration(device);
        }

        public static void Dispose()
        {
            TransformedPositionColorTextureVertexType.DisposeDeclaration();
        }
    }
}
