﻿using SharpDX.Direct3D9;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace HAJE.WowArena.Rendering
{
    class SpriteFactory : IDisposable
    {
        public SpriteFactory(Device graphicsDevice)
        {
            this.graphicsDevice = graphicsDevice;
        }

        public Sprite CreateSprite(string path)
        {
            Texture tex;
            Size size;
            if (textureCache.TryGetValue(path, out tex)
                && sizeCache.TryGetValue(path, out size))
            {
                return new Sprite(tex, size);
            }
            
            int width = 0, height = 0;
            using (Image image = Image.FromFile(path))
            {
                width = image.Width;
                height = image.Height;
            }
            tex = Texture.FromFile(graphicsDevice, path);
            size = new Size(width, height);
            textureCache.Add(path, tex);
            sizeCache.Add(path, size);
            return new Sprite(tex, size);
        }

        public void Dispose()
        {
            foreach (Texture t in textureCache.Values)
            {
                t.Dispose();
            }
            textureCache.Clear();
            sizeCache.Clear();
        }

        Device graphicsDevice;
        Dictionary<string, Texture> textureCache = new Dictionary<string, Texture>();
        Dictionary<string, Size> sizeCache = new Dictionary<string, Size>();
    }
}
