﻿using System.Runtime.InteropServices;

namespace HAJE.WowArena
{
    public class GameTime
    {
        #region dll imports

        [DllImport("kernel32.dll", CallingConvention = CallingConvention.Winapi)]
        private static extern bool QueryPerformanceFrequency(out long frequency);

        [DllImport("kernel32.dll", CallingConvention = CallingConvention.Winapi)]
        private static extern bool QueryPerformanceCounter(out long counter);

        #endregion

        public GameTime()
        {
            QueryPerformanceCounter(out tick);
            startTick = oldTick = tick;
            breakPointThresholdInTick = breakPointDeltaTick = 0;
            UpdateOtherRepresentations();
        }

        public Second DeltaTime
        {
            get
            {
                return deltaTime;
            }
        }

        public void Refresh()
        {
            oldTick = tick;
            QueryPerformanceCounter(out tick);
            if ((breakPointThresholdInTick > 0)
                && (tick - oldTick > breakPointThresholdInTick))
            {
                oldTick = tick - breakPointDeltaTick;
            }
            UpdateOtherRepresentations();
        }

        public void SetDeltaTime(Second deltaTime)
        {
            oldTick = tick - (long)(deltaTime * frequency);
            UpdateOtherRepresentations();
        }

        public void SetBreakPointSafeValue(Second threshold, Second deltaTime)
        {
            breakPointThresholdInTick = (long)(frequency * threshold);
            breakPointDeltaTick = (long)(frequency * deltaTime);
        }

        #region privates

        private void UpdateOtherRepresentations()
        {
            if (tick <= oldTick) tick = oldTick + 1;
            deltaTime = (Second)((float)(tick - oldTick) / frequency);
        }

        static GameTime()
        {
            QueryPerformanceFrequency(out frequency);
        }

        private static long frequency;
        private long startTick;
        private long tick;
        private long oldTick;
        private long breakPointThresholdInTick;
        private long breakPointDeltaTick;

        private Second deltaTime;

        #endregion
    }
}
