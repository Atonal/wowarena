﻿
namespace HAJE.WowArena
{
    public struct Second
    {
        public static explicit operator Second(float time)
        {
            return new Second(time);
        }

        public static implicit operator float(Second time)
        {
            return time.value;
        }

        #region operator

        public static Second operator -(Second lhs, Second rhs)
        {
            return (Second)(lhs.value - rhs.value);
        }

        public static Second operator *(Second lhs, float rhs)
        {
            return (Second)(lhs.value * rhs);
        }

        public static Second operator *(float lhs, Second rhs)
        {
            return (Second)(lhs * rhs.value);
        }

        #endregion

        public override string ToString()
        {
            return value + "seconds";
        }

        #region privates

        private float value;

        private Second(float value)
        {
            this.value = value;
        }

        #endregion
    }
}
