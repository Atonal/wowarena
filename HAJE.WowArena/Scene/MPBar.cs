﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HAJE.WowArena.Rendering;
using Sprite = HAJE.WowArena.Rendering.Sprite;
using SharpDX;
using SharpDX.Direct3D9;



namespace HAJE.WowArena.Scene
{
    class MPBar
    {
        readonly Sprite fg_image;
        readonly Sprite bg_image;
        readonly Sprite fill_image;
        const int width = 500; 

        public MPBar(SpriteFactory factory, Device device)
        {
            System.Diagnostics.Debug.Assert(width > 0);

            fg_image = factory.CreateSprite("Resource/mana-bar-fg.png");
            bg_image = factory.CreateSprite("Resource/mana-bar-bg.png");
            fill_image = factory.CreateSprite("Resource/mana-bar-fill.png");

            fill_image.Scale = new Vector2(0, 1);



            var fontDesc = new FontDescription()
            {
                CharacterSet = FontCharacterSet.Ansi | FontCharacterSet.Hangul,
                FaceName = "굴림체",
                Height = 14,
                Italic = false,
                Weight = FontWeight.Bold,
                MipLevels = 0,
                OutputPrecision = FontPrecision.TrueType,
                PitchAndFamily = FontPitchAndFamily.Default,
                Quality = FontQuality.ClearType
            };
            font = new Font(device, fontDesc);


            //redpartimage.Anchor = frameimage.Anchor = new Vector2(0.5f, 0f);
            //redpartimage.Anchor = Vector2.Zero;
        }

        public void Draw(SpriteRenderer render)
        {
            render.Draw(fg_image);
            render.Draw(bg_image);
            render.Draw(fill_image);
            font.DrawText(null, curMP.ToString() + " / " + maxMP.ToString(), 480, 713, Color.White);
        }


        float acctime = 0;
        public void Update(GameTime gameTime)
        {
            //경고: 이 안에서 MPValue 속성을 수정하면 무한 재귀에 빠집니다. MPvalue를 수정하세요.

            if (gameTime != null && MPvalue != lastvalue) { acctime += gameTime.DeltaTime; }


            const float tick = 0.008f;
            for (; acctime > tick; acctime -= tick)
            {
                if (Math.Abs(MPvalue - lastvalue) < 0.002f)
                {
                    lastvalue = MPvalue;
                    acctime = 0;
                }
                fill_image.Scale = new Vector2(lastvalue, 1);
                lastvalue = (lastvalue * 0.98f + MPvalue * 0.02f);
            }
        }


        public Vector2 Position
        {
            get
            {
                return fg_image.Position;
            }
            set
            {
                bg_image.Position = value;
                fill_image.Position = value + new Vector2(27, 0);
                fg_image.Position = value;
            }
        }

        public Vector2 Anchor
        {
            get
            {
                return fg_image.Anchor;
            }
            set
            {
                fg_image.Anchor = value;
                bg_image.Anchor = value;
                fill_image.Anchor = value;
            }
        }




        private Font font;



        // managing max / current MP

        int maxMP = 400;
        int curMP = 0;

        float MPvalue = 0f;
        float lastvalue = 0f;


        public int max_MP
        {
            get
            {
                return maxMP;
            }
            set
            {
                maxMP = value;
            }

        }

        public int cur_MP
        {
            get
            {
                return curMP;
            }
            set
            {
                curMP = value;
                MPvalue = (float)curMP / (float)maxMP;
                Update(null);
            }
        }
    }
}
