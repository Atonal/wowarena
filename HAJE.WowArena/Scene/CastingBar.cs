﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HAJE.WowArena.Rendering;
using SharpDX;

namespace HAJE.WowArena.Scene
{
    class CastingBar
    {
        readonly Sprite yellowpartimage;
        readonly Sprite frameimage;
        const int width = 164;
        public bool Visible
        {
            get
            {
                return yellowpartimage.Visible;
            }
            set
            {
                yellowpartimage.Visible = value;
                frameimage.Visible = value;
            }
        }
        //float castingTime;

        public Second CastingTime
        {
            get;
            set;
        }

        Second progress;
        public Second Progress
        {
            get
            {
                return progress;
            }
            private set
            {
                progress = value;
                Update(null);
                
            }
        }

        ///<summary>
        ///진행 상황을 0으로 만들고, 현재 보이지 않는다면 보이게 바꿉니다. 시전 시간이 인수로 주어져 있다면 그 값으로 재설정합니다.
        ///</summary>
        public void Reset()
        {
            Progress = (Second)0f;
            Visible = true;
        }

        ///<summary>
        ///진행 상황을 0으로 만들고, 현재 보이지 않는다면 보이게 바꿉니다. 시전 시간이 인수로 주어져 있다면 그 값으로 재설정합니다.
        ///</summary>
        public void Reset(Second castingTime)
        {
            CastingTime = castingTime;
            Reset();
        }

        public CastingBar(SpriteFactory factory) : this((Second)5f, factory) { }

        public CastingBar(Second castingTime, SpriteFactory factory)
        {
            
            
            yellowpartimage = factory.CreateSprite("Resource/CastingBarYellowPart.png");
            frameimage = factory.CreateSprite("Resource/CastingBarFrame.png");

            Visible = false;
            CastingTime = castingTime;
            
            //redpartimage.Anchor = frameimage.Anchor = new Vector2(0.5f, 0f);
            //redpartimage.Anchor = Vector2.Zero;
        }

        public void Draw(SpriteRenderer render)
        {
            yellowpartimage.Draw(render);
            frameimage.Draw(render);

        }

        //Vector2 position = Vector2.Zero;

        /*
        public Vector2 Anchor
        {
            get
            {
                return frameimage.Anchor;
            }
            set
            {
                frameimage.Anchor = value;
                redpartimage.Anchor = new Vector2(value;
            }
        }
         */

        
        public void Update(GameTime gameTime)
        {
            //경고: 이 안에서 Progress 속성을 수정하면 무한 재귀에 빠집니다. progress를 수정하세요.
            if (!Visible) return;

            if (gameTime != null) { progress += gameTime.DeltaTime; }
            if (progress >= CastingTime)
            {
                progress = CastingTime;
                //Visible = false;
            }

            yellowpartimage.Scale = new Vector2(progress / CastingTime, 1);


        }

        public Vector2 Position
        {
            get
            {
                return frameimage.Position;
            }
            set
            {
                frameimage.Position = value;
                yellowpartimage.Position = value + new Vector2(3, 0);
            }
        }

        
        
    }
}
