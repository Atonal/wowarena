﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HAJE.WowArena.Rendering;
using Sprite = HAJE.WowArena.Rendering.Sprite;
using SharpDX;
using Character = HAJE.WowArena.Simulation.Character;
using SharpDX.Direct3D9;


namespace HAJE.WowArena.Scene
{
    class BuffDebuff
    {
        public BuffDebuff(SpriteFactory spriteFactory)
        {
            //this.classEnum = classEnum;
            this.spriteFactory = spriteFactory;


        }

        public void AttachBuff(String buffname)
        {
            for (int i = 0; i < BuffIndex; i++)
            {
                if (curBuff[i] == buffname) return;
            }
            curBuff[BuffIndex] = buffname;
            curBuffSprite[BuffIndex] = spriteFactory.CreateSprite(String.Format("Resource/SkillIcon/{0}.png", buffname));
            curBuffSprite[BuffIndex].Scale = new Vector2(0.5f, 0.5f);
            curBuffSprite[BuffIndex].Anchor = anchor;
            curBuffSprite[BuffIndex].Position = curBuffPos[BuffIndex];
            BuffIndex++;
        }

        public void DetachBuff(String buffname)
        {
            for (int i = 0; i < BuffIndex; i++)
            {
                if (curBuff[i] == buffname)
                {
                    for (int j = i+1; j < BuffIndex; j++)
                    {
                        curBuff[j-1] = curBuff[j];
                        curBuffSprite[j-1] = curBuffSprite[j];
                        curBuffSprite[j - 1].Position = curBuffPos[j-1];
                    }
                    curBuff[BuffIndex] = null;
                    curBuffSprite[BuffIndex] = null;
                    break;
                }
            }
            BuffIndex--;

        }

        public Vector2 Position
        {
            get
            {
                return curBuffPos[0];
            }
            set
            {
                curBuffPos[0] = value;
                curBuffPos[1] = value + new Vector2(33, 0);
                curBuffPos[2] = value + new Vector2(65, 0);
                curBuffPos[3] = value + new Vector2(0, 33);
                curBuffPos[4] = value + new Vector2(33, 33);
                curBuffPos[5] = value + new Vector2(65, 33);
            }
        }

        public Vector2 Anchor
        {
            get
            {
                return anchor;
            }
            set
            {
                anchor = value;
            }
        }

        public void Draw(SpriteRenderer render)
        {
            for (int i = 0; i < BuffIndex; i++)
            {
                render.Draw(curBuffSprite[i]);
            }

        }


        Vector2 anchor = new Vector2 (0.5f, 0.5f);
        String[] curBuff = new String[6];
        Sprite[] curBuffSprite = new Sprite[6];
        Vector2[] curBuffPos = new Vector2[6];
        int BuffIndex = 0;
        SpriteFactory spriteFactory;
        private Font font;
    }
}
