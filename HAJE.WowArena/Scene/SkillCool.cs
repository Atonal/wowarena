﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HAJE.WowArena.Rendering;
using Sprite = HAJE.WowArena.Rendering.Sprite;
using SharpDX;
using Character = HAJE.WowArena.Simulation.Character;
using SharpDX.Direct3D9;




namespace HAJE.WowArena.Scene
{
    class SkillCool
    {
       
        readonly Sprite Skill_icon;
        readonly Sprite Skill_cool;

        public SkillCool(SpriteFactory spriteFactory, int classEnum, Device device)
        {
            this.classEnum = classEnum;
            Skill_icon = spriteFactory.CreateSprite(String.Format("Resource/SkillIcon/icon_{0}_cool.png", Character.className[classEnum]));
            Skill_cool = spriteFactory.CreateSprite(String.Format("Resource/SkillIcon/icon_{0}_cool_c.png", Character.className[classEnum]));

            var fontDesc = new FontDescription()
            {
                CharacterSet = FontCharacterSet.Ansi | FontCharacterSet.Hangul,
                FaceName = "굴림체",
                Height = 35,
                Italic = false,
                Weight = FontWeight.Bold,
                MipLevels = 0,
                OutputPrecision = FontPrecision.TrueType,
                PitchAndFamily = FontPitchAndFamily.Default,
                Quality = FontQuality.ClearType
            };
            font = new Font(device, fontDesc);

        }


        /*
        public bool Hover
        {
            get { return hover; }
            set { hover = value; }
        }

        public bool Selected
        {
            get { return selected; }
            set { selected = value; }
        }

        public void PlayHitAnimation()
        {

        }

        public void PlayAttackAnimation()
        {

        }         
         */




        /*
        public bool Cool
        {
            get { return active;  }
            set { cool = value; }
        }
         */

        public float Cooldown
        {
            get { return cooldown; }
            set
            {
                cooldown = value;
                if (cooldown > 0) cool = true;
                else cool = false;
            }
        }

        
        
        public Vector2 Position
       {
           get
           {
               return Skill_icon.Position;
           }
           set
           {
               Skill_icon.Position = value;
               Skill_cool.Position = value;
           }
       }
 
        public Vector2 Anchor
        {
            get
            {
                return Skill_icon.Anchor;
            }
            set
            {
                Skill_icon.Anchor = value;
                Skill_cool.Anchor = value;
            }
        }

        public void Draw(SpriteRenderer render)
        {
            if (!cool) render.Draw(Skill_icon);
            else render.Draw(Skill_cool);

            if (cool) font.DrawText(null, ((int)cooldown).ToString(), (int)Skill_icon.Position.X - 10, (int)Skill_icon.Position.Y-15, Color.Red);
        }

        private Font font;
        bool cool = false;
        float cooldown = 0f;

        int classEnum;
        SpriteFactory spriteFactory;
    }
}
