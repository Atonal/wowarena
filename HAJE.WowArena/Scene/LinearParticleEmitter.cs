﻿using HAJE.WowArena.Rendering;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HAJE.WowArena.Scene
{
    class LinearParticleEmitter : IUpdatable
    {
        public LinearParticleEmitter(Scheduler scheduler, ParticleSystem particleSystem)
        {
            this.owner = particleSystem;
            this.scheduler = scheduler;
        }

        public Vector2 From;
        public Vector2 To;
        public float EmitterSpeed = 750.0f;
        public Color ColorFrom = Color.Red;
        public Color ColorTo = Color.Orange;
        public float MinParticleSpeed = 200.0f;
        public float MaxParticleSpeed = 300.0f;
        public Second ParticleMinLife = (Second)0.15f;
        public Second ParticleMaxLife = (Second)0.25f;
        public float ParticlesPerSecond = 100.0f;
        public float MinSize = 10;
        public float MaxSize = 14;

        public void Start()
        {
            scheduler.Register(this);

            Vector2 dist = To - From;
            emitterLife = (Second)(dist.Length() / EmitterSpeed);
            if (float.IsNaN(emitterLife)) emitterLife = (Second)1.0f;
            scheduler.ScheduleOnce(End, emitterLife);
            emitterLifeRemains = emitterLife;

            particleInterval = (Second)(1.0f / ParticlesPerSecond);
            timeToCreate = (Second)0;
        }

        public void Update(GameTime gameTime)
        {
            emitterLifeRemains -= gameTime.DeltaTime;
            float fromRatio = emitterLifeRemains / emitterLife;
            Vector2 position = From * fromRatio + To * (1 - fromRatio);

            timeToCreate -= gameTime.DeltaTime;
            while (timeToCreate <= 0)
            {
                timeToCreate += particleInterval;
                owner.AddParticle(CreateParticle(position));
            }
        }

        public void End()
        {
            scheduler.Hide(this);
        }

        Particle CreateParticle(Vector2 position)
        {
            Particle p;
            p.Position = position;
            float cRatio = rand.NextFloat(0.0f, 1.0f);
            p.Color = new Color(ColorFrom.ToVector4() * cRatio + ColorTo.ToVector4() * (1 - cRatio));
            p.Acceleration = Vector2.Zero;
            p.Size = rand.NextFloat(MinSize, MaxSize);
            p.LifeRemain = (Second)rand.NextFloat(ParticleMinLife, ParticleMaxLife);

            Radian t = (Radian)rand.NextFloat(Radian.Pi * 2);
            p.Velocity = t.Rotate(new Vector2(rand.NextFloat(MinParticleSpeed, MaxParticleSpeed), 0));
            return p;
        }

        ParticleSystem owner;
        Scheduler scheduler;
        Random rand = new Random();
        Second emitterLife;
        Second emitterLifeRemains;
        Second timeToCreate;
        Second particleInterval;
    }
}
