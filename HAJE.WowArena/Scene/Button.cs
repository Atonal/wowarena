﻿using HAJE.WowArena.Rendering;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HAJE.WowArena.Scene
{
    class Button
    {
        public bool Visible
        {
            get
            {
                return normalImage.Visible;
            }
            set
            {
                normalImage.Visible = value;
                overImage.Visible = value;
            }
        }

        public Button(string normalPath, string overPath, SpriteFactory factory)
        {
            normalImage = factory.CreateSprite(normalPath);
            overImage = factory.CreateSprite(overPath);
        }

        public void Update(GameTime gameTime)
        {
            if (GameSystem.Mouse.IsButtonPressed(Input.MouseButtons.Left))
            {
                isOver = CheckHit(GameSystem.Mouse.Position);
            }
            else
            {
                if (isOver)
                {
                    OnClicked();
                }
                isOver = false;
            }
        }

        public int Tag = 0;

        public event Action OnClicked = delegate { };

        public Vector2 Position
        {
            get
            {
                return normalImage.Position;
            }
            set
            {
                normalImage.Position = value;
                overImage.Position = value;
            }
        }

        public Vector2 Anchor
        {
            get
            {
                return normalImage.Anchor;
            }
            set
            {
                normalImage.Anchor = value;
                overImage.Anchor = value;
            }
        }

        public Vector2 Scale
        {
            get
            {
                return normalImage.Scale;
            }
            set
            {
                normalImage.Scale = value;
                overImage.Scale = value;
            }
        }

        bool CheckHit(Vector2 mousePosition)
        {
            var tl = normalImage.TopLeft;
            var br = normalImage.BottomRight;
            if (tl.X < mousePosition.X && mousePosition.X < br.X
                && tl.Y < mousePosition.Y && mousePosition.Y < br.Y)
            {
                return true;
            }
            return false;
        }

        public void Draw(SpriteRenderer render)
        {
            if (isOver)
            {
                overImage.Draw(render);
            }
            else
            {
                normalImage.Draw(render);
            }
        }

        Sprite normalImage;
        Sprite overImage;
        bool isOver = false;
    }
}
