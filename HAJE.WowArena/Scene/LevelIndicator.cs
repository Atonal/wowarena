﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HAJE.WowArena.Rendering;
using SharpDX;

namespace HAJE.WowArena.Scene
{
    class LevelIndicator
    {
        readonly Sprite[] indicators = new Sprite[5];

        public bool Visible
        {
            get
            {
                return indicators[0].Visible;
            }
            set
            {
                for (int i = 0; i < 5; i++) { indicators[i].Visible = value; }
            }
        }

        public LevelIndicator(SpriteFactory factory)
            : this(1, factory) { }

        public int Level
        {
            get;
            set;
        }

        public LevelIndicator(int level, SpriteFactory factory)
        {
            for (int i = 0; i < 5; i++)
            {
                indicators[i] = factory.CreateSprite("Resource/Level" + (i + 1).ToString() + ".png");                
            }
            Scale = Vector2.One; // new Vector2(0.75f, 0.75f);
            Level = level;
        }

        

        public Vector2 Position
        {
            get
            {
                return indicators[0].Position;
            }
            set
            {
                for (int i = 0; i < 5; i++) { indicators[i].Position = value; }
            }
        }

        public Vector2 Scale
        {
            get
            {
                return indicators[0].Scale;
            }
            set
            {
                for (int i = 0; i < 5; i++) { indicators[i].Scale = value; }
            }
        }

        public Vector2 Anchor
        {
            get
            {
                return indicators[0].Anchor;
            }
            set
            {
                for (int i = 0; i < 5; i++) { indicators[i].Anchor = value; }
            }
        }

        public void Draw(SpriteRenderer render)
        {
            indicators[Level - 1].Draw(render);
        }
    }
}
