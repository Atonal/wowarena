﻿using HAJE.WowArena.Network;
using HAJE.WowArena.Rendering;
using SharpDX;
using SharpDX.Direct3D9;
using Sprite = HAJE.WowArena.Rendering.Sprite;
using Character = HAJE.WowArena.Simulation.Character;
using System.Diagnostics;
using HAJE.WowArena.Input;
using HAJE.WowArena.Simulation;


namespace HAJE.WowArena.Scene
{
    public class ArenaScene : IUpdatable
    {
        
        
        public void Start()
        {
            GameSystem.UpdateLoop.Register(this);
            spriteFactory = new SpriteFactory(GameSystem.GraphicsDevice);
            renderer = new SpriteRenderer(GameSystem.GraphicsDevice);
            background = spriteFactory.CreateSprite("Resource/background.png");
            readyButton = new Button(
                "Resource/btn_ready_normal.png",
                "Resource/btn_ready_over.png",
                spriteFactory);
            readyButton.Position = new Vector2(1024, 768) / 2;
            readyButton.Anchor = new Vector2(0.5f, 0.5f);
            readyButton.OnClicked += OnReadyButton;

            cancelButton = new Button(
                "Resource/btn_cancel_normal.png",
                "Resource/btn_cancel_over.png",
                spriteFactory);
            cancelButton.Position = new Vector2(1024, 768) / 2;
            cancelButton.Anchor = new Vector2(0.5f, 0.5f);
            cancelButton.OnClicked += OnCancelButton;

            session = Network.Session.RunningSession;

            const float resizefactor = 1.2f;

            //아군 원딜
            allyHeroes[Character.Mage] = new CharacterSprite(Character.Mage, new Vector2(144, 525), new Vector2(0.6f, 0.6f) * resizefactor, spriteFactory, GameSystem.GraphicsDevice, true);
            allyHeroes[Character.Priest] = new CharacterSprite(Character.Priest, new Vector2(512, 500), new Vector2(0.6f, 0.6f) * resizefactor, spriteFactory, GameSystem.GraphicsDevice, true);
            allyHeroes[Character.Hunter] = new CharacterSprite(Character.Hunter, new Vector2(880, 525), new Vector2(0.6f, 0.6f) * resizefactor, spriteFactory, GameSystem.GraphicsDevice, true);

            //아군 근딜
            allyHeroes[Character.Paladin] = new CharacterSprite(Character.Paladin, new Vector2(320, 450), new Vector2(0.58f, 0.58f) * resizefactor, spriteFactory, GameSystem.GraphicsDevice, true);
            allyHeroes[Character.Warrior] = new CharacterSprite(Character.Warrior, new Vector2(688, 450), new Vector2(0.58f, 0.58f) * resizefactor, spriteFactory, GameSystem.GraphicsDevice, true);

            //적군 근딜
            enemyHeroes[Character.Paladin] = new CharacterSprite(Character.Paladin, new Vector2(688, 200), new Vector2(0.53f, 0.53f) * resizefactor, spriteFactory, GameSystem.GraphicsDevice, false);
            enemyHeroes[Character.Warrior] = new CharacterSprite(Character.Warrior, new Vector2(336, 200), new Vector2(0.53f, 0.53f) * resizefactor, spriteFactory, GameSystem.GraphicsDevice, false);

            //적군 원딜
            enemyHeroes[Character.Mage] = new CharacterSprite(Character.Mage, new Vector2(864, 150), new Vector2(0.5f, 0.5f) * resizefactor, spriteFactory, GameSystem.GraphicsDevice, false);
            enemyHeroes[Character.Priest] = new CharacterSprite(Character.Priest, new Vector2(504, 120), new Vector2(0.5f, 0.5f) * resizefactor, spriteFactory, GameSystem.GraphicsDevice, false);
            enemyHeroes[Character.Hunter] = new CharacterSprite(Character.Hunter, new Vector2(152, 150), new Vector2(0.5f, 0.5f) * resizefactor, spriteFactory, GameSystem.GraphicsDevice, false);

            

            for (int i = 0; i < 5; i++)
            {

                allyHPbar[i] = new HPBar(spriteFactory, GameSystem.GraphicsDevice);
                allyHPbar[i].Position = allyHeroes[i].Sprite.Position + new Vector2(-83, 256 * allyHeroes[i].Sprite.Scale.Y * resizefactor * 0.4f);
                allylevelindicator[i] = new LevelIndicator(spriteFactory);
                allylevelindicator[i].Position = allyHeroes[i].Sprite.Position + new Vector2(85, 256 * allyHeroes[i].Sprite.Scale.Y * resizefactor * 0.4f + 10);
                allycastingbar[i] = new CastingBar(spriteFactory);
                allycastingbar[i].Position = allyHeroes[i].Sprite.Position + new Vector2(-83, -256 * allyHeroes[i].Sprite.Scale.Y * resizefactor * 0.4f - 14);
                allySkillCool[i] = new SkillCool(spriteFactory, i, GameSystem.GraphicsDevice);
                allySkillCool[i].Position = allyHeroes[i].Sprite.Position + new Vector2(0, 450 * allyHeroes[i].Sprite.Scale.Y * resizefactor * 0.4f);
                allyBuffDebuff[i] = new BuffDebuff(spriteFactory);
                allyBuffDebuff[i].Position = allyHeroes[i].Sprite.Position + new Vector2(-36, -240 * allyHeroes[i].Sprite.Scale.Y * resizefactor * 0.4f);


                enemyHPbar[i] = new HPBar(spriteFactory, GameSystem.GraphicsDevice);
                enemyHPbar[i].Position = enemyHeroes[i].Sprite.Position + new Vector2(-83, 256 * enemyHeroes[i].Sprite.Scale.Y * resizefactor * 0.4f);
                enemylevelindicator[i] = new LevelIndicator(spriteFactory);
                enemylevelindicator[i].Position = enemyHeroes[i].Sprite.Position + new Vector2(85, 256 * enemyHeroes[i].Sprite.Scale.Y * resizefactor * 0.4f + 7);
                enemycastingbar[i] = new CastingBar(spriteFactory);
                enemycastingbar[i].Position = enemyHeroes[i].Sprite.Position + new Vector2(-83, -256 * enemyHeroes[i].Sprite.Scale.Y * resizefactor * 0.4f - 14);
                enemyBuffDebuff[i] = new BuffDebuff(spriteFactory);
                enemyBuffDebuff[i].Position = enemyHeroes[i].Sprite.Position + new Vector2(-36, -240 * allyHeroes[i].Sprite.Scale.Y * resizefactor * 0.4f);

                allyHeroes[i].Sprite.Anchor = enemyHeroes[i].Sprite.Anchor = allylevelindicator[i].Anchor = enemylevelindicator[i].Anchor = allySkillCool[i].Anchor = new Vector2(.5f, .5f);

                allyHeroes[i].Sprite.Visible = allycastingbar[i].Visible = allyHPbar[i].Visible = allylevelindicator[i].Visible =
                enemyHeroes[i].Sprite.Visible = enemycastingbar[i].Visible = enemyHPbar[i].Visible = enemylevelindicator[i].Visible = false;
            }

            allyMPbar = new MPBar(spriteFactory, GameSystem.GraphicsDevice);
            allyMPbar.Position = new Vector2(262, 700);
            particleSystem = new ParticleSystem(GameSystem.GraphicsDevice, "Resource/particle.png");

            GameSystem.Keyboard.KeyUp += OnKeyDown;
            GameSystem.Mouse.Click += OnMouseClick;
        }

        public void Update(GameTime gameTime)
        {
            if (isReadyButtonShowing)
            {
                if (isReady) cancelButton.Update(gameTime);
                else readyButton.Update(gameTime);
            }

            if (gameRunning)
            {
                for (int i = 0; i < 5; i++)
                {
                    allyHPbar[i].Update(gameTime);
                    enemyHPbar[i].Update(gameTime);
                    
                    allycastingbar[i].Update(gameTime);
                    enemycastingbar[i].Update(gameTime);
                    allyMPbar.Update(gameTime);

                    allyHeroes[i].Update(gameTime);
                    enemyHeroes[i].Update(gameTime);

                    allyHeroes[i].UpdateDamageMessage(gameTime);
                    enemyHeroes[i].UpdateDamageMessage(gameTime);
                }

                CheckMouseOver();
            }

            particleSystem.Update(gameTime);

            Draw();

            scheduler.Update(gameTime);
        }

        public void Draw()
        {
            GameSystem.GraphicsDevice.BeginScene();
            GameSystem.GraphicsDevice.Clear(ClearFlags.Target | ClearFlags.ZBuffer, new ColorBGRA(0, 0, 0, 0), 1.0f, 0);

            renderer.SetUp();
            background.Draw(renderer);
            //testbar.Draw(renderer);

            //if (testbar.HPValue > 0) testbar.HPValue -= 0.001f;

            if (gameRunning)
            {

                for (int i = 0; i < 5; i++)
                {
                    allyHeroes[i].Sprite.Draw(renderer);
                    enemyHeroes[i].Sprite.Draw(renderer);
                    allyHPbar[i].Draw(renderer);
                    enemyHPbar[i].Draw(renderer);
                    allylevelindicator[i].Draw(renderer);
                    enemylevelindicator[i].Draw(renderer);
                    allycastingbar[i].Draw(renderer);
                    enemycastingbar[i].Draw(renderer);
                    allyMPbar.Draw(renderer);
                    allySkillCool[i].Draw(renderer);
                    allyHeroes[i].DrawDamageMessages(renderer);
                    enemyHeroes[i].DrawDamageMessages(renderer);
                    allyBuffDebuff[i].Draw(renderer);
                    enemyBuffDebuff[i].Draw(renderer);
                }
            }


            if (isReadyButtonShowing)
            {
                if (isReady) cancelButton.Draw(renderer);
                else readyButton.Draw(renderer);
            }

            particleSystem.Draw();

            GameSystem.DebugOutput.Draw();
            GameSystem.GraphicsDevice.EndScene();
            GameSystem.GraphicsDevice.Present();
        }

        public void End()
        {
            GameSystem.Keyboard.KeyUp -= OnKeyDown;
            GameSystem.Mouse.Click -= OnMouseClick;
            GameSystem.UpdateLoop.Hide(this);
            spriteFactory.Dispose();
            spriteFactory = null;
            renderer.Dispose();
            renderer = null;
            particleSystem.Dispose();
            particleSystem = null;
        }

        #region ready cancel button

        public void ShowReadyButton()
        {
            isReady = false;
            isReadyButtonShowing = true;
        }

        void OnReadyButton()
        {
            session.SetReady(true);
            isReady = true;
        }

        void OnCancelButton()
        {
            session.SetReady(false);
            isReady = false;
        }

        bool isReadyButtonShowing = false;
        bool isReady = false;
        Button readyButton;
        Button cancelButton;

        #endregion

        bool gameRunning = false;

        public void StartGame()
        {
            gameRunning = true;
            isReadyButtonShowing = false;

            //scheduler.ScheduleOnce(() => { allycastingbar[2].Reset((Second)6f); }, (Second)5);
            //scheduler.ScheduleOnce(() => { allyHPbar[3].cur_HP = 80; }, (Second)2);
            //scheduler.ScheduleOnce(() => { allySkillCool[3].Cooldown = 1f; }, (Second)3);
            /*
            scheduler.ScheduleOnce(() => { allyBuffDebuff[3].AttachBuff("hunter_passive"); }, (Second)3);
            scheduler.ScheduleOnce(() => { allyBuffDebuff[3].AttachBuff("mage_passive"); }, (Second)4);
            scheduler.ScheduleOnce(() => { allyBuffDebuff[3].AttachBuff("priest_passive"); }, (Second)5);
            scheduler.ScheduleOnce(() => { allyBuffDebuff[3].AttachBuff("warrior_passive"); }, (Second)6);
            scheduler.ScheduleOnce(() => { allyBuffDebuff[3].AttachBuff("hunter_passive"); }, (Second)7);
            scheduler.ScheduleOnce(() => { allyBuffDebuff[3].AttachBuff("paladin_passive"); }, (Second)8);
            scheduler.ScheduleOnce(() => { allyBuffDebuff[3].DetachBuff("mage_passive"); }, (Second)9);
            */
        }

        public void PlaceCharacter(int player, int character, int maxHp)
        {

            int heroindex = character;

            if (Helper.IsMine(player))
            {
                allyHeroes[heroindex].Sprite.Visible = true;
                allyHPbar[heroindex].Visible = true;
                allyHPbar[heroindex].max_HP = maxHp;
                allyHPbar[heroindex].cur_HP = maxHp;
                allylevelindicator[heroindex].Visible = true;
            }
            else
            {
                enemyHeroes[heroindex].Sprite.Visible = true;
                enemyHPbar[heroindex].Visible = true;
                enemyHPbar[heroindex].max_HP = maxHp;
                enemyHPbar[heroindex].cur_HP = maxHp;
                enemylevelindicator[heroindex].Visible = true;
            }
            
        }

        public void SetMana(int player, int value)
        {
            if (Helper.IsMine(player))
                allyMPbar.cur_MP = value;
        }

        public void RegenerateMana(int player, int regenValue, int manaAfter)
        {
            if (Helper.IsMine(player))
                allyMPbar.cur_MP = manaAfter;
        }

        public void Attack(int attackingPlayer, int attacker, int target, int skill,
            int damage, bool isMainTarget, bool isCritial, int hpAfter)
        {
            if (Helper.IsMine(attackingPlayer))
            {
                enemyHPbar[target].cur_HP = hpAfter;

                enemyHeroes[target].DamageMessages.Add(new CharacterSprite.DamageMessage(damage.ToString(), enemyHeroes[target].Sprite.Position, enemyHeroes[target].RemovePool));

                // 공격,피격 애니메이션 재생
                allyHeroes[attacker].PlayAttackAnimation();
                enemyHeroes[target].PlayHitAnimation();

                // 파티클 애니메이션 재생
                allyHeroes[attacker].PlayParticleAnimation(enemyHeroes[target], scheduler, particleSystem);
            }
            else
            {
                allyHPbar[target].cur_HP = hpAfter;

                allyHeroes[target].DamageMessages.Add(new CharacterSprite.DamageMessage(damage.ToString(), allyHeroes[target].Sprite.Position, allyHeroes[target].RemovePool));

                // 공격,피격 애니메이션 재생
                enemyHeroes[attacker].PlayAttackAnimation();
                allyHeroes[target].PlayHitAnimation();
            }
        }

        public void InstantCast(int player, int caster, int cost, int manaAfter)
        {
            if (Helper.IsMine(player))
            {
                allyMPbar.cur_MP = manaAfter;
            }
        }

        public void BeginCast(int player, int caster, float time, int cost, int manaAfter)
        {
            if (Helper.IsMine(player))
            {
                allycastingbar[caster].Reset((Second)time);
                allyMPbar.cur_MP = manaAfter;
            }
            else
            {
                enemycastingbar[caster].Reset((Second)time);
            }
        }

        public void CancelCast(int player, int caster)
        {
            if (Helper.IsMine(player))
            {
                allycastingbar[caster].Visible = false;
            }
        }

        public void EndCast(int player, int caster)
        {
            if (Helper.IsMine(player))
            {
                allycastingbar[caster].Visible = false;
            }
            else
            {
                enemycastingbar[caster].Visible = false;
            }
        }

        public void AttachEffect(int player, int target, int effect, int value, float lifeTime)
        {
            if (Helper.IsMine(player))
            {
                allyBuffDebuff[player].AttachBuff(EffectToString(effect));
            }
            else
            {
                enemyBuffDebuff[player].AttachBuff(EffectToString(effect));
            }
        }

        public void DetachEffect(int player, int target, int effect)
        {
            if (Helper.IsMine(player))
            {
                allyBuffDebuff[target].DetachBuff(EffectToString(effect));
            }
            else
            {
                enemyBuffDebuff[player].AttachBuff(EffectToString(effect));
            }
        }

        public static string EffectToString(int effect)
        {
            switch(effect)
            {
                case Simulation.Effect.Aimed:
                    return "icon_hunter_passive";
                case Simulation.Effect.Concentrated:
                    return "icon_hunter_hit";
                case Simulation.Effect.Intervene:
                    return "icon_warrior_cool";
                case Simulation.Effect.Vengence:
                    return "icon_warrior_passive";
                case Simulation.Effect.DeepFreeze:
                    return "icon_mage_cool";
                case Simulation.Effect.DivineProtection:
                    return "icon_paladin_cool";
                case Simulation.Effect.Renew:
                    return "icon_priest_cool";
                case Simulation.Effect.EchoOfLight:
                    return "icon_priest_passive";
                case Simulation.Effect.InterveneSelf:
                    return "icon_warrior_cool";
                default: return null;
            }
        }

        public void UpgradeBegin(int player, int target, float time, int cost, int manaAfter)
        {
            if (Helper.IsMine(player))
            {
                allycastingbar[target].Reset((Second)time);
                allyMPbar.cur_MP = manaAfter;
            }
            
        }

        public void UpgradeEnd(int player, int target, int levelAfter, int hpAfter, int hpMaxAfter)
        {
            if (Helper.IsMine(player))
            {
                allycastingbar[target].Visible = false;
                allylevelindicator[target].Level = levelAfter;
                allyHPbar[target].cur_HP = hpAfter;
                allyHPbar[target].max_HP = hpMaxAfter;
            }
        }

        public void UpgradeCancelled(int player, int target, int refund, int manaAfter)
        {
            if (Helper.IsMine(player))
            {
                allycastingbar[target].Visible = false;
                allyMPbar.cur_MP = manaAfter;
            }
        }

        public void Heal(int player, int healer, int target, int skill,
            int healAmount, bool isMainTarget, bool isCritial, int hpAfter)
        {
            if (Helper.IsMine(player))
            {
                allyHPbar[target].cur_HP = hpAfter;
            }
            else
            {
                enemyHPbar[target].cur_HP = hpAfter;
            }
        }

       
            //return character;

            /*
             */

        void OnKeyDown(Keys key)
        {
            if (key == Keys.D1)
            {
                SetSelectedCharacter(Character.Mage);
            }
            else if (key == Keys.D2)
            {
                SetSelectedCharacter(Character.Paladin);
            }
            else if (key == Keys.D3)
            {
                SetSelectedCharacter(Character.Priest);
            }
            else if (key == Keys.D4)
            {
                SetSelectedCharacter(Character.Warrior);
            }
            else if (key == Keys.D5)
            {
                SetSelectedCharacter(Character.Hunter);
            }
            else if (key == Keys.A)
            {
                selectionState = SelectionState.AttackTarget;
            }
            else if (key == Keys.U)
            {
                if (selectedCharacter >= 0)
                {
                    session.Upgrade(selectedCharacter);
                }
            }
            else if (key == Keys.S)
            {
                selectionState = SelectionState.SkillTarget;
            }
            else if (key == Keys.Escape)
            {
                selectionState = SelectionState.None;
            }
        }

        void SetSelectedCharacter(int character)
        {
            if (selectedCharacter >= 0)
            {
                allyHeroes[selectedCharacter].Selected = false;
            }
            if (character >= 0)
            {
                allyHeroes[character].Selected = true;
            }
            selectedCharacter = character;
            selectionState = SelectionState.None;
        }

        void CheckMouseOver()
        {
            if (selectionState == SelectionState.None
                || selectionState == SelectionState.CharacterSelect)
            {
                SetOverCharacter(-1, false);
                return;
            }

            var mousePos = GameSystem.Mouse.Position;
            bool isEnemyTarget = true;
            bool isFriendTarget = false;
            if (selectionState == SelectionState.AttackTarget)
            {
                if (selectedCharacter == Character.Priest)
                {
                    isEnemyTarget = false;
                    isFriendTarget = true;
                }
                else if (selectedCharacter == Character.Paladin)
                {
                    isEnemyTarget = true;
                    isFriendTarget = true;
                }
            }
            else if (selectionState == SelectionState.SkillTarget)
            {
                if (selectedCharacter == Character.Priest
                    || selectedCharacter == Character.Paladin
                    || selectedCharacter == Character.Warrior)
                {
                    isEnemyTarget = false;
                    isFriendTarget = true;
                }
            }

            if (isEnemyTarget)
            {
                for (int i = 0; i < Character.Max; i++)
                {
                    if (Vector2.Distance(enemyHeroes[i].Sprite.Position, mousePos) < 100.0f)
                    {
                        SetOverCharacter(i, false);
                        return;
                    }
                }
            }
            
            if (isFriendTarget)
            {
                for (int i = 0; i < Character.Max; i++)
                {
                    if (Vector2.Distance(allyHeroes[i].Sprite.Position, mousePos) < 100.0f)
                    {
                        SetOverCharacter(i, true);
                        return;
                    }
                }
            }

            SetOverCharacter(-1, false);
        }

        void SetOverCharacter(int character, bool isAlly)
        {
            if (overCharacter >= 0)
            {
                if (overAlly)
                {
                    allyHeroes[overCharacter].Hover = false;
                }
                else
                {
                    enemyHeroes[overCharacter].Hover = false;
                }
            }

            overCharacter = character;
            overAlly = isAlly;

            if (overCharacter >= 0)
            {
                if (overAlly)
                {
                    allyHeroes[overCharacter].Hover = true;
                }
                else
                {
                    enemyHeroes[overCharacter].Hover = true;
                }
            }
        }

        void OnMouseClick(MouseButtons btn, int x, int y)
        {
            if (overCharacter >= 0)
            {
                if (selectionState == SelectionState.AttackTarget)
                {
                    session.SetTarget(selectedCharacter, overAlly, overCharacter);
                    selectionState = SelectionState.None;
                }
                else if (selectionState == SelectionState.SkillTarget)
                {
                    session.UseSkill(selectedCharacter, overCharacter);
                    selectionState = SelectionState.None;
                }
            }
        }

        Sprite background;
        CharacterSprite[] allyHeroes = new CharacterSprite[5];
        CharacterSprite[] enemyHeroes = new CharacterSprite[5];
        HPBar[] allyHPbar = new HPBar[5];
        HPBar[] enemyHPbar = new HPBar[5];
        MPBar allyMPbar;
        LevelIndicator[] allylevelindicator = new LevelIndicator[5];
        LevelIndicator[] enemylevelindicator = new LevelIndicator[5];
        CastingBar[] allycastingbar = new CastingBar[5];
        CastingBar[] enemycastingbar = new CastingBar[5];
        SkillCool[] allySkillCool = new SkillCool[5];
        BuffDebuff[] allyBuffDebuff = new BuffDebuff[5];
        BuffDebuff[] enemyBuffDebuff = new BuffDebuff[5];
        
        Scheduler scheduler = new Scheduler();

        SpriteRenderer renderer;
        SpriteFactory spriteFactory;
        Session session;

        ParticleSystem particleSystem;

        int selectedCharacter = -1;
        enum SelectionState
        {
            None = 0,
            CharacterSelect,
            AttackTarget,
            SkillTarget,
        }
        int overCharacter = -1;
        bool overAlly = false;
        SelectionState selectionState = SelectionState.CharacterSelect;
    }
}
