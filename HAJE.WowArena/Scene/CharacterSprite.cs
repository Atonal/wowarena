﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using SharpDX.Direct3D9;
using HAJE.WowArena.Rendering;
using System.Diagnostics;
using Sprite = HAJE.WowArena.Rendering.Sprite;
using Character = HAJE.WowArena.Simulation.Character;

namespace HAJE.WowArena.Scene
{
    class CharacterSprite
    {
        public CharacterSprite(int classEnum, Vector2 position, Vector2 scale, SpriteFactory spriteFactory, Device device, bool isPlayers)
        {
            this.isPlayers = isPlayers;
            this.classEnum = classEnum;
            this.basePosition = position;
            this.baseScale = scale;
            this.normal = spriteFactory.CreateSprite(String.Format("Resource/character_{0}.png", Character.className[classEnum]));
            this.over = spriteFactory.CreateSprite(String.Format("Resource/character_{0}_over.png", Character.className[classEnum]));
            this.select = spriteFactory.CreateSprite(String.Format("Resource/character_{0}_select.png", Character.className[classEnum]));
            select.Position = over.Position = normal.Position = basePosition;
            select.Scale = over.Scale = normal.Scale = baseScale;
            select.Anchor = over.Anchor = new Vector2(.5f, .5f);
            this.viewing = normal;


            var fontDesc = new FontDescription()
            {
                CharacterSet = FontCharacterSet.Ansi | FontCharacterSet.Hangul,
                FaceName = "Times New Roman",
                Height = 24,
                Italic = false,
                Weight = FontWeight.Bold,
                MipLevels = 0,
                OutputPrecision = FontPrecision.TrueType,
                PitchAndFamily = FontPitchAndFamily.Default,
                Quality = FontQuality.ClearType
            };
            font = new Font(device, fontDesc);
            
        }

        public class DamageMessage
        {
            public float elapsedTime;
            float acctime = 0;
            //public Color TextColor;
            public Vector2 Position
            {
                get;
                set;
            }
            readonly Vector2 startingPosition;
            public readonly string Message;
            List<DamageMessage> removePool;

            public DamageMessage(string message, Vector2 position, List<DamageMessage> removePool)
            {
                Message = message;
                elapsedTime = 0f;
                Random random = new Random();
                Position = startingPosition = position + random.NextVector2(new Vector2(-50, -50), new Vector2(50, 50));
                //TextColor = Color.Red;
                this.removePool = removePool;
            }

            public void Update(GameTime gametime)
            {

                Position = startingPosition - new Vector2(0f, elapsedTime / 0.01f);
                //TextColor = new Color((Vector3)Color.Red, elapsedTime / 0.4f);

                elapsedTime += gametime.DeltaTime;

                if (elapsedTime >= 0.8f)
                    removePool.Add(this);
            }

        }
        public Font font;
        public List<DamageMessage> DamageMessages = new List<DamageMessage>();
        public List<DamageMessage> RemovePool = new List<DamageMessage>();

        public void UpdateDamageMessage(GameTime gameTime)
        {

            foreach (DamageMessage damageMessage in DamageMessages)
            {
                damageMessage.Update(gameTime);
            }
            foreach (DamageMessage damageMessage in RemovePool)
            {
                DamageMessages.Remove(damageMessage);
                
            }
            RemovePool.Clear();
        }

        public void DrawDamageMessages(SpriteRenderer render)
        {
            foreach (DamageMessage damageMessage in DamageMessages)
            {
                font.DrawText(null, damageMessage.Message, (int)damageMessage.Position.X, (int)damageMessage.Position.Y, Color.Red);
            }
        }

        #region
        public bool Hover
        {
            get { return hover; }
            set { hover = value; }
        }

        public bool Selected
        {
            get { return selected; }
            set { selected = value; }
        }

        public Sprite Sprite
        {
            get { return viewing; }
            set { viewing = value; }
        }
        #endregion

        public void Update(GameTime gameTime)
        {
            currentTime += gameTime.DeltaTime;

            if (selected) viewing = select;
            else if (hover) viewing = over;
            else viewing = normal;

            ProcessAnimation();
        }

        public void ProcessAnimation()
        {
            // 공격 애니메이션
            if (isPlayAttackAnimation)
            {
                float animationDuration = Helper.attackAnimationDuration;
                float animationTime = currentTime - attackAnimationStartTime;

                if (animationTime > animationDuration)
                { 
                    isPlayAttackAnimation = false;
                    viewing.Position = basePosition;
                    return;
                }
                
                float attackDistance = 80.0f;
                float teamFactor = isPlayers ? -1.0f: 1.0f;

                float ease = Helper.EaseInOutQuad(animationTime, animationDuration);
                ease = ease <= 0.5f ? ease : 1.0f - ease;
                viewing.Position = basePosition + new Vector2(0.0f, attackDistance * ease * teamFactor);
            }

            // 피격 애니메이션
            //TODO
        }

        public void PlayParticleAnimation(CharacterSprite target, Scheduler scheduler, ParticleSystem particleSystem)
        {
            // 좌표 계산
            Vector2 startPoint = basePosition;// +Helper.characterSpriteSize * baseScale / 2.0f;
            Vector2 endPoint = target.basePosition;// +Helper.characterSpriteSize * target.baseScale / 2.0f;

            var particle = new LinearParticleEmitter(scheduler, particleSystem);
            particle.From = startPoint;
            particle.To = endPoint;

            System.Random rand = new System.Random();
            particle.ColorFrom = new Color(rand.Next(165, 255), rand.Next(165, 255), 0, rand.Next(100, 255));
            particle.ColorTo = new Color(255, 0, 0, rand.Next(100, 255));
            particle.ParticlesPerSecond = 10000.0f;
            particle.ParticleMinLife = (Second)0.035f;
            particle.ParticleMaxLife = (Second)0.07f;
            particle.MinSize = 3;
            particle.MaxSize = 5;
            particle.MinParticleSpeed = 300.0f;
            particle.MaxParticleSpeed = 900.0f;


            particle.Start();
        }

        public void PlayHitAnimation()
        {
            hitAnimationStartTime = currentTime;
            isPlayHitAnimation = true;
        }

        public void PlayAttackAnimation()
        {
            attackAnimationStartTime = currentTime;
            isPlayAttackAnimation = true;
        }

        float currentTime = 0.0f;
        float hitAnimationStartTime;
        float attackAnimationStartTime;

        bool isPlayAttackAnimation = false;
        bool isPlayHitAnimation = false;

        bool hover = false;
        bool selected = false;

        bool isPlayers;
        int classEnum;
        Vector2 baseScale;
        Vector2 basePosition;
        Sprite viewing;
        Sprite normal;
        Sprite over;
        Sprite select;
    }
}
