﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HAJE.WowArena.Network;

namespace HAJE.WowArena.Scene
{
    static class Helper
    {
        public static bool IsMine(int player)
        {
            return Session.RunningSession.IsHost == (player == Simulation.Player.Host);
        }

        public const float attackAnimationDuration = 0.35f; // 단위: 초
        public const float hitAnimationDuration = 1.0f; // 단위: 초
        public const int characterSpriteSize = 256;

        public static float EaseInOutQuad(float time, float duration)
        {
            float t = time / (duration / 2.0f);
            if (t < 1)
            {
                return 0.5f * t * t;
            }
            else
            {
                t--;
                return -0.5f * (t * (t - 2.0f) - 1.0f);
            }
        }
    }
}
