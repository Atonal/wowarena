﻿using SharpDX;
using SharpDX.Direct3D9;
using HAJE.WowArena.Rendering;
using Sprite = HAJE.WowArena.Rendering.Sprite;
using Character = HAJE.WowArena.Simulation.Character;

namespace HAJE.WowArena.Scene
{
    /// <summary>
    /// 접속중인 상태에 보여지는 화면
    /// </summary>
    class ConnectionScene : IUpdatable
    {
        public void Start()
        {
            GameSystem.UpdateLoop.Register(this);

            spriteFactory = new SpriteFactory(GameSystem.GraphicsDevice);
            renderer = new SpriteRenderer(GameSystem.GraphicsDevice);

            heroes[Character.Hunter] = spriteFactory.CreateSprite("Resource/ConnectionScene/hunter.png");
            heroes[Character.Warrior] = spriteFactory.CreateSprite("Resource/ConnectionScene/warrior.png");
            heroes[Character.Mage] = spriteFactory.CreateSprite("Resource/ConnectionScene/mage.png");
            heroes[Character.Paladin] = spriteFactory.CreateSprite("Resource/ConnectionScene/paladin.png");
            heroes[Character.Priest] = spriteFactory.CreateSprite("Resource/ConnectionScene/priest.png");

            System.Random rand = new System.Random();
            backgroundIndex = rand.Next(0, 5);
        }

        public void Update(GameTime gameTime)
        {
            Draw();
        }

        public void Draw()
        {
            GameSystem.GraphicsDevice.BeginScene();
            GameSystem.GraphicsDevice.Clear(ClearFlags.Target | ClearFlags.ZBuffer, new ColorBGRA(0, 0, 0, 0), 1.0f, 0);

            renderer.SetUp();
            heroes[backgroundIndex].Draw(renderer);

            GameSystem.DebugOutput.Draw();
            GameSystem.GraphicsDevice.EndScene();
            GameSystem.GraphicsDevice.Present();
        }


        public void End()
        {
            GameSystem.UpdateLoop.Hide(this);
            spriteFactory.Dispose();
            spriteFactory = null;
            renderer.Dispose();
            renderer = null;
        }

        int backgroundIndex;
        Sprite[] heroes = new Sprite[5];

        Scheduler scheduler = new Scheduler();
        SpriteFactory spriteFactory;
        SpriteRenderer renderer;

    }
}
