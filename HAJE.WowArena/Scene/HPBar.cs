﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HAJE.WowArena.Rendering;
using Sprite = HAJE.WowArena.Rendering.Sprite;
using SharpDX;
using SharpDX.Direct3D9;


namespace HAJE.WowArena.Scene
{
    class HPBar
    {
        readonly Sprite redpartimage;
        readonly Sprite frameimage;
        const int width = 165;

        public bool Visible
        {
            get
            {
                return redpartimage.Visible;
            }
            set
            {
                redpartimage.Visible = value;
                frameimage.Visible = value;
            }
        }

        public HPBar(SpriteFactory factory, Device device)
        {
            System.Diagnostics.Debug.Assert(width > 0);

            redpartimage = factory.CreateSprite("Resource/HPBarRedpart.png");
            frameimage = factory.CreateSprite("Resource/HPBarFrame.png");



            var fontDesc = new FontDescription()
            {
                CharacterSet = FontCharacterSet.Ansi | FontCharacterSet.Hangul,
                FaceName = "굴림체",
                Height = 14,
                Italic = false,
                Weight = FontWeight.Bold,
                MipLevels = 0,
                OutputPrecision = FontPrecision.TrueType,
                PitchAndFamily = FontPitchAndFamily.Default,
                Quality = FontQuality.ClearType
            };
            font = new Font(device, fontDesc);



            //redpartimage.Anchor = frameimage.Anchor = new Vector2(0.5f, 0f);
            //redpartimage.Anchor = Vector2.Zero;
        }

        public void Draw(SpriteRenderer render)
        {
            redpartimage.Draw(render);
            frameimage.Draw(render);
            font.DrawText(null, curHP.ToString() + " / " + maxHP.ToString(), (int)frameimage.Position.X + 40, (int)frameimage.Position.Y + 5, Color.White);

            int x = (int)frameimage.Position.X + 62;
        }

        //Vector2 position = Vector2.Zero;

        /*
        public Vector2 Anchor
        {
            get
            {
                return frameimage.Anchor;
            }
            set
            {
                frameimage.Anchor = value;
                redpartimage.Anchor = new Vector2(value;
            }
        }
         */

        float acctime = 0;
        public void Update(GameTime gameTime)
        {
            //경고: 이 안에서 curHP 속성을 수정하면 무한 재귀에 빠집니다. curHP를 수정하세요.

            if (gameTime != null && HPvalue != lastvalue) { acctime += gameTime.DeltaTime; }


            const float tick = 0.008f;
            for (; acctime > tick; acctime -= tick)
            {
                if (Math.Abs(HPvalue - lastvalue) < 0.002f)
                {
                    lastvalue = HPvalue;
                    acctime = 0;
                }
                redpartimage.Scale = new Vector2(lastvalue, 1);
                lastvalue = (lastvalue * 0.98f + HPvalue * 0.02f);
            }
        }

        public Vector2 Position
        {
            get
            {
                return frameimage.Position;
            }
            set
            {
                frameimage.Position = value;
                redpartimage.Position = value + new Vector2(3, 0);
            }
        }





        private Font font;





        int maxHP = 100;
        int curHP = 100;


        float HPvalue = 1f;
        float lastvalue = 1f;

        public int max_HP
        {
            get
            {
                return maxHP;
            }
            set
            {
                maxHP = value;
                HPvalue = (float)curHP / (float)maxHP;
                Update(null);
            }

        }

        public int cur_HP
        {
            get
            {
                return curHP;
            }
            set
            {
                curHP = value;
                HPvalue = (float)curHP / (float)maxHP;
                Update(null);
            }
        }
    }
}
